<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    public function run()
    {
        $roles = [
            [
                'id'    => 1,
                'title' => 'Admin',
            ],
            [
                'id'    => 2,
                'title' => 'كاتب عام',
            ],
            [
                'id'    => 3,
                'title' => 'كاتب عام مساعد',
            ],
            [
                'id'    => 4,
                'title' => 'امين مال',
            ],
            [
                'id'    => 5,
                'title' => 'مكلف بالنخراطات',
            ],
            [
                'id'    => 6,
                'title' => 'مكلف بالاتصال',
            ],
            [
                'id'    => 7,
                'title' => 'عضو مكتب',
            ],
            [
                'id'    => 8,
                'title' => 'منخرط',
            ],
        ];

        Role::insert($roles);
    }
}
