<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $users = [
            [
                'id'             => 1,
                'name'           => 'Admin',
                'email'          => 'admin@admin.com',
                'password'       => '$2y$10$xq.cc1JqN.WU2HCWmOuObu1PdfrWfu9lpmGLjvb7e7AqHSdlvZZ6e',
                'remember_token' => null,
                'firstname'      => '',
                'lastname'       => '',
                'state'          => '',
                'street'         => '',
                'street_number'  => '',
                'telephone'      => '',
                'approved'       => 1,
            ],
        ];

        User::insert($users);
    }
}
