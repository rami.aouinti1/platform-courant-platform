<?php

use App\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $permissions = [
            [
                'id'    => '1',
                'title' => 'user_management_access',
            ],
            [
                'id'    => '2',
                'title' => 'permission_create',
            ],
            [
                'id'    => '3',
                'title' => 'permission_edit',
            ],
            [
                'id'    => '4',
                'title' => 'permission_show',
            ],
            [
                'id'    => '5',
                'title' => 'permission_delete',
            ],
            [
                'id'    => '6',
                'title' => 'permission_access',
            ],
            [
                'id'    => '7',
                'title' => 'role_create',
            ],
            [
                'id'    => '8',
                'title' => 'role_edit',
            ],
            [
                'id'    => '9',
                'title' => 'role_show',
            ],
            [
                'id'    => '10',
                'title' => 'role_delete',
            ],
            [
                'id'    => '11',
                'title' => 'role_access',
            ],
            [
                'id'    => '12',
                'title' => 'user_create',
            ],
            [
                'id'    => '13',
                'title' => 'user_edit',
            ],
            [
                'id'    => '14',
                'title' => 'user_show',
            ],
            [
                'id'    => '15',
                'title' => 'user_delete',
            ],
            [
                'id'    => '16',
                'title' => 'user_access',
            ],
            [
                'id'    => '17',
                'title' => 'task_management_access',
            ],
            [
                'id'    => '18',
                'title' => 'task_status_create',
            ],
            [
                'id'    => '19',
                'title' => 'task_status_edit',
            ],
            [
                'id'    => '20',
                'title' => 'task_status_show',
            ],
            [
                'id'    => '21',
                'title' => 'task_status_delete',
            ],
            [
                'id'    => '22',
                'title' => 'task_status_access',
            ],
            [
                'id'    => '23',
                'title' => 'task_tag_create',
            ],
            [
                'id'    => '24',
                'title' => 'task_tag_edit',
            ],
            [
                'id'    => '25',
                'title' => 'task_tag_show',
            ],
            [
                'id'    => '26',
                'title' => 'task_tag_delete',
            ],
            [
                'id'    => '27',
                'title' => 'task_tag_access',
            ],
            [
                'id'    => '28',
                'title' => 'task_create',
            ],
            [
                'id'    => '29',
                'title' => 'task_edit',
            ],
            [
                'id'    => '30',
                'title' => 'task_show',
            ],
            [
                'id'    => '31',
                'title' => 'task_delete',
            ],
            [
                'id'    => '32',
                'title' => 'task_access',
            ],
            [
                'id'    => '33',
                'title' => 'tasks_calendar_access',
            ],
            [
                'id'    => '34',
                'title' => 'profile_create',
            ],
            [
                'id'    => '35',
                'title' => 'profile_edit',
            ],
            [
                'id'    => '36',
                'title' => 'profile_show',
            ],
            [
                'id'    => '37',
                'title' => 'profile_delete',
            ],
            [
                'id'    => '38',
                'title' => 'profile_access',
            ],
            [
                'id'    => '39',
                'title' => 'rapport_create',
            ],
            [
                'id'    => '40',
                'title' => 'rapport_edit',
            ],
            [
                'id'    => '41',
                'title' => 'rapport_show',
            ],
            [
                'id'    => '42',
                'title' => 'rapport_delete',
            ],
            [
                'id'    => '43',
                'title' => 'rapport_access',
            ],
            [
                'id'    => '44',
                'title' => 'rapport_financier_create',
            ],
            [
                'id'    => '45',
                'title' => 'rapport_financier_edit',
            ],
            [
                'id'    => '46',
                'title' => 'rapport_financier_show',
            ],
            [
                'id'    => '47',
                'title' => 'rapport_financier_delete',
            ],
            [
                'id'    => '48',
                'title' => 'rapport_financier_access',
            ],
            [
                'id'    => '49',
                'title' => 'contact_management_access',
            ],
            [
                'id'    => '50',
                'title' => 'contact_company_create',
            ],
            [
                'id'    => '51',
                'title' => 'contact_company_edit',
            ],
            [
                'id'    => '52',
                'title' => 'contact_company_show',
            ],
            [
                'id'    => '53',
                'title' => 'contact_company_delete',
            ],
            [
                'id'    => '54',
                'title' => 'contact_company_access',
            ],
            [
                'id'    => '55',
                'title' => 'contact_contact_create',
            ],
            [
                'id'    => '56',
                'title' => 'contact_contact_edit',
            ],
            [
                'id'    => '57',
                'title' => 'contact_contact_show',
            ],
            [
                'id'    => '58',
                'title' => 'contact_contact_delete',
            ],
            [
                'id'    => '59',
                'title' => 'contact_contact_access',
            ],
            [
                'id'    => '60',
                'title' => 'basic_c_r_m_access',
            ],
            [
                'id'    => '61',
                'title' => 'crm_status_create',
            ],
            [
                'id'    => '62',
                'title' => 'crm_status_edit',
            ],
            [
                'id'    => '63',
                'title' => 'crm_status_show',
            ],
            [
                'id'    => '64',
                'title' => 'crm_status_delete',
            ],
            [
                'id'    => '65',
                'title' => 'crm_status_access',
            ],
            [
                'id'    => '66',
                'title' => 'crm_customer_create',
            ],
            [
                'id'    => '67',
                'title' => 'crm_customer_edit',
            ],
            [
                'id'    => '68',
                'title' => 'crm_customer_show',
            ],
            [
                'id'    => '69',
                'title' => 'crm_customer_delete',
            ],
            [
                'id'    => '70',
                'title' => 'crm_customer_access',
            ],
            [
                'id'    => '71',
                'title' => 'crm_note_create',
            ],
            [
                'id'    => '72',
                'title' => 'crm_note_edit',
            ],
            [
                'id'    => '73',
                'title' => 'crm_note_show',
            ],
            [
                'id'    => '74',
                'title' => 'crm_note_delete',
            ],
            [
                'id'    => '75',
                'title' => 'crm_note_access',
            ],
            [
                'id'    => '76',
                'title' => 'crm_document_create',
            ],
            [
                'id'    => '77',
                'title' => 'crm_document_edit',
            ],
            [
                'id'    => '78',
                'title' => 'crm_document_show',
            ],
            [
                'id'    => '79',
                'title' => 'crm_document_delete',
            ],
            [
                'id'    => '80',
                'title' => 'crm_document_access',
            ],
            [
                'id'    => '81',
                'title' => 'studium_create',
            ],
            [
                'id'    => '82',
                'title' => 'studium_edit',
            ],
            [
                'id'    => '83',
                'title' => 'studium_show',
            ],
            [
                'id'    => '84',
                'title' => 'studium_delete',
            ],
            [
                'id'    => '85',
                'title' => 'studium_access',
            ],
            [
                'id'    => '86',
                'title' => 'experience_create',
            ],
            [
                'id'    => '87',
                'title' => 'experience_edit',
            ],
            [
                'id'    => '88',
                'title' => 'experience_show',
            ],
            [
                'id'    => '89',
                'title' => 'experience_delete',
            ],
            [
                'id'    => '90',
                'title' => 'experience_access',
            ],
            [
                'id'    => '91',
                'title' => 'certificate_create',
            ],
            [
                'id'    => '92',
                'title' => 'certificate_edit',
            ],
            [
                'id'    => '93',
                'title' => 'certificate_show',
            ],
            [
                'id'    => '94',
                'title' => 'certificate_delete',
            ],
            [
                'id'    => '95',
                'title' => 'certificate_access',
            ],
            [
                'id'    => '96',
                'title' => 'other_create',
            ],
            [
                'id'    => '97',
                'title' => 'other_edit',
            ],
            [
                'id'    => '98',
                'title' => 'other_show',
            ],
            [
                'id'    => '99',
                'title' => 'other_delete',
            ],
            [
                'id'    => '100',
                'title' => 'other_access',
            ],
            [
                'id'    => '101',
                'title' => 'news_create',
            ],
            [
                'id'    => '102',
                'title' => 'news_edit',
            ],
            [
                'id'    => '103',
                'title' => 'news_show',
            ],
            [
                'id'    => '104',
                'title' => 'news_delete',
            ],
            [
                'id'    => '105',
                'title' => 'news_access',
            ],
        ];

        Permission::insert($permissions);
    }
}
