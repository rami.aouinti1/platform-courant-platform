<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCertificateUserPivotTable extends Migration
{
    public function up()
    {
        Schema::create('certificate_user', function (Blueprint $table) {
            $table->unsignedInteger('user_id');

            $table->foreign('user_id', 'user_id_fk_782464')->references('id')->on('users')->onDelete('cascade');

            $table->unsignedInteger('certificate_id');

            $table->foreign('certificate_id', 'certificate_id_fk_782464')->references('id')->on('certificates')->onDelete('cascade');
        });
    }
}
