<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudiumUserPivotTable extends Migration
{
    public function up()
    {
        Schema::create('studium_user', function (Blueprint $table) {
            $table->unsignedInteger('user_id');

            $table->foreign('user_id', 'user_id_fk_782462')->references('id')->on('users')->onDelete('cascade');

            $table->unsignedInteger('studium_id');

            $table->foreign('studium_id', 'studium_id_fk_782462')->references('id')->on('studia')->onDelete('cascade');
        });
    }
}
