<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name')->nullable();

            $table->string('email')->nullable()->unique();

            $table->datetime('email_verified_at')->nullable();

            $table->string('password')->nullable();

            $table->string('remember_token')->nullable();

            $table->string('firstname')->nullable();

            $table->string('lastname')->nullable();

            $table->date('date_of_birthday')->nullable();

            $table->integer('carte_number')->nullable();

            $table->string('country')->nullable();

            $table->string('state')->nullable();

            $table->string('street')->nullable();

            $table->string('street_number')->nullable();

            $table->string('telephone')->nullable();

            $table->date('date_beginn')->nullable();

            $table->string('sexe')->nullable();

            $table->string('status')->nullable();

            $table->boolean('approved')->default(0)->nullable();

            $table->timestamps();

            $table->softDeletes();
        });
    }
}
