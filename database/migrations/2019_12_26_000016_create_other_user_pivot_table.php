<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOtherUserPivotTable extends Migration
{
    public function up()
    {
        Schema::create('other_user', function (Blueprint $table) {
            $table->unsignedInteger('user_id');

            $table->foreign('user_id', 'user_id_fk_782465')->references('id')->on('users')->onDelete('cascade');

            $table->unsignedInteger('other_id');

            $table->foreign('other_id', 'other_id_fk_782465')->references('id')->on('others')->onDelete('cascade');
        });
    }
}
