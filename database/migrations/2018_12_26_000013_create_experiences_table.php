<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExperiencesTable extends Migration
{
    public function up()
    {
        Schema::create('experiences', function (Blueprint $table) {
            $table->increments('id');

            $table->string('company_name')->nullable();

            $table->string('description')->nullable();

            $table->date('date_beginn')->nullable();

            $table->date('date_end')->nullable();

            $table->timestamps();

            $table->softDeletes();
        });
    }
}
