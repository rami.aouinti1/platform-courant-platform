<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Experience extends Model
{
    use SoftDeletes;

    public $table = 'experiences';

    protected $dates = [
        'date_end',
        'created_at',
        'updated_at',
        'deleted_at',
        'date_beginn',
    ];

    protected $fillable = [
        'date_end',
        'created_at',
        'updated_at',
        'deleted_at',
        'description',
        'date_beginn',
        'company_name',
    ];

    public function experienceUsers()
    {
        return $this->belongsToMany(User::class);
    }

    public function getDateBeginnAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('panel.date_format')) : null;
    }

    public function setDateBeginnAttribute($value)
    {
        $this->attributes['date_beginn'] = $value ? Carbon::createFromFormat(config('panel.date_format'), $value)->format('Y-m-d') : null;
    }

    public function getDateEndAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('panel.date_format')) : null;
    }

    public function setDateEndAttribute($value)
    {
        $this->attributes['date_end'] = $value ? Carbon::createFromFormat(config('panel.date_format'), $value)->format('Y-m-d') : null;
    }
}
