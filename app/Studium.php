<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Studium extends Model
{
    use SoftDeletes;

    public $table = 'studia';

    protected $dates = [
        'date_end',
        'created_at',
        'updated_at',
        'deleted_at',
        'date_beginn',
    ];

    protected $fillable = [
        'ecole',
        'degree',
        'branche',
        'date_end',
        'created_at',
        'updated_at',
        'deleted_at',
        'date_beginn',
    ];

    const DEGREE_SELECT = [
        'Ecole primaire'   => 'Ecole primaire',
        'Ecole secondaire' => 'Ecole secondaire',
        'Licence'          => 'Licence',
        'Formation'        => 'Formation',
        'Master'           => 'Master',
        'Doctorat'         => 'Doctorat',
    ];

    public function studiumUsers()
    {
        return $this->belongsToMany(User::class);
    }

    public function getDateBeginnAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('panel.date_format')) : null;
    }

    public function setDateBeginnAttribute($value)
    {
        $this->attributes['date_beginn'] = $value ? Carbon::createFromFormat(config('panel.date_format'), $value)->format('Y-m-d') : null;
    }

    public function getDateEndAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('panel.date_format')) : null;
    }

    public function setDateEndAttribute($value)
    {
        $this->attributes['date_end'] = $value ? Carbon::createFromFormat(config('panel.date_format'), $value)->format('Y-m-d') : null;
    }
}
