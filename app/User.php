<?php

namespace App;

use App\Notifications\VerifyUserNotification;
use Carbon\Carbon;
use Hash;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class User extends Authenticatable implements HasMedia
{
    use SoftDeletes, Notifiable, HasApiTokens, HasMediaTrait;

    public $table = 'users';

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $appends = [
        'photo',
    ];

    const SEXE_SELECT = [
        'Homme' => 'Homme',
        'Femme' => 'Femme',
    ];

    const COUNTRY_SELECT = [
        'Tunisia' => 'Tunisia',
        'Germany' => 'Germany',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'date_beginn',
        'date_of_birthday',
        'email_verified_at',
    ];

    const STATUS_SELECT = [
        'Etudiant'  => 'Etudiant',
        'Employee'  => 'Employee',
        'Formation' => 'Formation',
        'Chomeur'   => 'Chomeur',
    ];

    protected $fillable = [
        'name',
        'sexe',
        'state',
        'email',
        'status',
        'street',
        'country',
        'lastname',
        'password',
        'firstname',
        'telephone',
        'created_at',
        'updated_at',
        'deleted_at',
        'date_beginn',
        'carte_number',
        'street_number',
        'approved',
        'remember_token',
        'date_of_birthday',
        'email_verified_at',
    ];

    public function getIsAdminAttribute()
    {
        return $this->roles()->where('id', 1)->exists();
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);
    }

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        self::created(function (User $user) {
            $registrationRole = config('panel.registration_default_role');

            if (!$user->roles()->get()->contains($registrationRole)) {
                $user->roles()->attach($registrationRole);
            }
        });
    }

    public function assignedToTasks()
    {
        return $this->hasMany(Task::class, 'assigned_to_id', 'id');
    }

    public function userNews()
    {
        return $this->hasMany(News::class, 'user_id', 'id');
    }

    public function getEmailVerifiedAtAttribute($value)
    {
        return $value ? Carbon::createFromFormat('Y-m-d H:i:s', $value)->format(config('panel.date_format') . ' ' . config('panel.time_format')) : null;
    }

    public function setEmailVerifiedAtAttribute($value)
    {
        $this->attributes['email_verified_at'] = $value ? Carbon::createFromFormat(config('panel.date_format') . ' ' . config('panel.time_format'), $value)->format('Y-m-d H:i:s') : null;
    }

    public function setPasswordAttribute($input)
    {
        if ($input) {
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
        }
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function getPhotoAttribute()
    {
        $file = $this->getMedia('photo')->last();

        if ($file) {
            $file->url       = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
        }

        return $file;
    }

    public function getDateOfBirthdayAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('panel.date_format')) : null;
    }

    public function setDateOfBirthdayAttribute($value)
    {
        $this->attributes['date_of_birthday'] = $value ? Carbon::createFromFormat(config('panel.date_format'), $value)->format('Y-m-d') : null;
    }

    public function getDateBeginnAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('panel.date_format')) : null;
    }

    public function setDateBeginnAttribute($value)
    {
        $this->attributes['date_beginn'] = $value ? Carbon::createFromFormat(config('panel.date_format'), $value)->format('Y-m-d') : null;
    }

    public function studia()
    {
        return $this->belongsToMany(Studium::class);
    }

    public function experiences()
    {
        return $this->belongsToMany(Experience::class);
    }

    public function certificates()
    {
        return $this->belongsToMany(Certificate::class);
    }

    public function others()
    {
        return $this->belongsToMany(Other::class);
    }
}
