<?php

namespace App\Http\Requests;

use App\Studium;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateStudiumRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('studium_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'date_beginn' => [
                'date_format:' . config('panel.date_format'),
                'nullable',
            ],
            'date_end'    => [
                'date_format:' . config('panel.date_format'),
                'nullable',
            ],
        ];
    }
}
