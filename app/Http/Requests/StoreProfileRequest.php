<?php

namespace App\Http\Requests;

use App\Profile;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreProfileRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('profile_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'date_of_birthday' => [
                'date_format:' . config('panel.date_format'),
                'nullable',
            ],
            'date_beginn'      => [
                'date_format:' . config('panel.date_format'),
                'nullable',
            ],
            'date_end'         => [
                'date_format:' . config('panel.date_format'),
                'nullable',
            ],
            'user_id'          => [
                'required',
                'integer',
            ],
        ];
    }
}
