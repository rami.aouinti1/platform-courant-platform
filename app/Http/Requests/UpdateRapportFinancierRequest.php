<?php

namespace App\Http\Requests;

use App\RapportFinancier;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateRapportFinancierRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('rapport_financier_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'date' => [
                'date_format:' . config('panel.date_format'),
                'nullable',
            ],
        ];
    }
}
