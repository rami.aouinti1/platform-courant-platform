<?php

namespace App\Http\Requests;

use App\User;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateUserRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('user_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'name'             => [
                'required',
            ],
            'email'            => [
                'required',
                'unique:users,email,' . request()->route('user')->id,
            ],
            'roles.*'          => [
                'integer',
            ],
            'roles'            => [
                'required',
                'array',
            ],
            'date_of_birthday' => [
                'date_format:' . config('panel.date_format'),
                'nullable',
            ],
            'carte_number'     => [
                'nullable',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'date_beginn'      => [
                'date_format:' . config('panel.date_format'),
                'nullable',
            ],
            'studia.*'         => [
                'integer',
            ],
            'studia'           => [
                'array',
            ],
            'experiences.*'    => [
                'integer',
            ],
            'experiences'      => [
                'array',
            ],
            'certificates.*'   => [
                'integer',
            ],
            'certificates'     => [
                'array',
            ],
            'others.*'         => [
                'integer',
            ],
            'others'           => [
                'array',
            ],
        ];
    }
}
