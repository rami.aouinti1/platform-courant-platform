<?php

namespace App\Http\Requests;

use App\Studium;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyStudiumRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('studium_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:studia,id',
        ];
    }
}
