<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\StoreRapportRequest;
use App\Http\Requests\UpdateRapportRequest;
use App\Http\Resources\Admin\RapportResource;
use App\Rapport;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RapportApiController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('rapport_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new RapportResource(Rapport::all());
    }

    public function store(StoreRapportRequest $request)
    {
        $rapport = Rapport::create($request->all());

        if ($request->input('file', false)) {
            $rapport->addMedia(storage_path('tmp/uploads/' . $request->input('file')))->toMediaCollection('file');
        }

        return (new RapportResource($rapport))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Rapport $rapport)
    {
        abort_if(Gate::denies('rapport_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new RapportResource($rapport);
    }

    public function update(UpdateRapportRequest $request, Rapport $rapport)
    {
        $rapport->update($request->all());

        if ($request->input('file', false)) {
            if (!$rapport->file || $request->input('file') !== $rapport->file->file_name) {
                $rapport->addMedia(storage_path('tmp/uploads/' . $request->input('file')))->toMediaCollection('file');
            }
        } elseif ($rapport->file) {
            $rapport->file->delete();
        }

        return (new RapportResource($rapport))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Rapport $rapport)
    {
        abort_if(Gate::denies('rapport_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $rapport->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
