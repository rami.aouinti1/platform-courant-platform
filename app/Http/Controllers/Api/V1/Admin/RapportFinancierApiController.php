<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\StoreRapportFinancierRequest;
use App\Http\Requests\UpdateRapportFinancierRequest;
use App\Http\Resources\Admin\RapportFinancierResource;
use App\RapportFinancier;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RapportFinancierApiController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('rapport_financier_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new RapportFinancierResource(RapportFinancier::all());
    }

    public function store(StoreRapportFinancierRequest $request)
    {
        $rapportFinancier = RapportFinancier::create($request->all());

        if ($request->input('file', false)) {
            $rapportFinancier->addMedia(storage_path('tmp/uploads/' . $request->input('file')))->toMediaCollection('file');
        }

        return (new RapportFinancierResource($rapportFinancier))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(RapportFinancier $rapportFinancier)
    {
        abort_if(Gate::denies('rapport_financier_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new RapportFinancierResource($rapportFinancier);
    }

    public function update(UpdateRapportFinancierRequest $request, RapportFinancier $rapportFinancier)
    {
        $rapportFinancier->update($request->all());

        if ($request->input('file', false)) {
            if (!$rapportFinancier->file || $request->input('file') !== $rapportFinancier->file->file_name) {
                $rapportFinancier->addMedia(storage_path('tmp/uploads/' . $request->input('file')))->toMediaCollection('file');
            }
        } elseif ($rapportFinancier->file) {
            $rapportFinancier->file->delete();
        }

        return (new RapportFinancierResource($rapportFinancier))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(RapportFinancier $rapportFinancier)
    {
        abort_if(Gate::denies('rapport_financier_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $rapportFinancier->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
