<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreStudiumRequest;
use App\Http\Requests\UpdateStudiumRequest;
use App\Http\Resources\Admin\StudiumResource;
use App\Studium;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class StudiumApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('studium_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new StudiumResource(Studium::all());
    }

    public function store(StoreStudiumRequest $request)
    {
        $studium = Studium::create($request->all());

        return (new StudiumResource($studium))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Studium $studium)
    {
        abort_if(Gate::denies('studium_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new StudiumResource($studium);
    }

    public function update(UpdateStudiumRequest $request, Studium $studium)
    {
        $studium->update($request->all());

        return (new StudiumResource($studium))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Studium $studium)
    {
        abort_if(Gate::denies('studium_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $studium->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
