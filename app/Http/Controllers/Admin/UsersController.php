<?php

namespace App\Http\Controllers\Admin;

use App\Certificate;
use App\Experience;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyUserRequest;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Other;
use App\Role;
use App\Studium;
use App\User;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class UsersController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('user_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $users = User::all();

        return view('admin.users.index', compact('users'));
    }

    public function members()
    {
        abort_if(Gate::denies('user_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $users = User::all();

        return view('admin.users.members', compact('users'));
    }

    public function profile()
    {

        $users = User::all();

        return view('admin.profiles.profile', compact('users'));
    }

    public function create()
    {
        abort_if(Gate::denies('user_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $roles = Role::all()->pluck('title', 'id');

        $studia = Studium::all()->pluck('ecole', 'id');

        $experiences = Experience::all()->pluck('company_name', 'id');

        $certificates = Certificate::all()->pluck('certificate', 'id');

        $others = Other::all()->pluck('title', 'id');

        return view('admin.users.create', compact('roles', 'studia', 'experiences', 'certificates', 'others'));
    }

    public function store(StoreUserRequest $request)
    {
        $user = User::create($request->all());
        $user->roles()->sync($request->input('roles', []));
        $user->studia()->sync($request->input('studia', []));
        $user->experiences()->sync($request->input('experiences', []));
        $user->certificates()->sync($request->input('certificates', []));
        $user->others()->sync($request->input('others', []));

        if ($request->input('photo', false)) {
            $user->addMedia(storage_path('tmp/uploads/' . $request->input('photo')))->toMediaCollection('photo');
        }

        return redirect()->route('admin.users.index');
    }

    public function edit(User $user)
    {
        abort_if(Gate::denies('user_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $roles = Role::all()->pluck('title', 'id');

        $studia = Studium::all()->pluck('ecole', 'id');

        $experiences = Experience::all()->pluck('company_name', 'id');

        $certificates = Certificate::all()->pluck('certificate', 'id');

        $others = Other::all()->pluck('title', 'id');

        $user->load('roles', 'studia', 'experiences', 'certificates', 'others');

        return view('admin.users.edit', compact('roles', 'studia', 'experiences', 'certificates', 'others', 'user'));
    }

    public function update(UpdateUserRequest $request, User $user)
    {
        $user->update($request->all());
        $user->roles()->sync($request->input('roles', []));
        $user->studia()->sync($request->input('studia', []));
        $user->experiences()->sync($request->input('experiences', []));
        $user->certificates()->sync($request->input('certificates', []));
        $user->others()->sync($request->input('others', []));

        if ($request->input('photo', false)) {
            if (!$user->photo || $request->input('photo') !== $user->photo->file_name) {
                $user->addMedia(storage_path('tmp/uploads/' . $request->input('photo')))->toMediaCollection('photo');
            }
        } elseif ($user->photo) {
            $user->photo->delete();
        }

        return redirect()->route('admin.users.index');
    }

    public function show(User $user)
    {
        abort_if(Gate::denies('user_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $user->load('roles', 'studia', 'experiences', 'certificates', 'others');

        return view('admin.users.show', compact('user'));
    }

    public function destroy(User $user)
    {
        abort_if(Gate::denies('user_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $user->delete();

        return back();
    }

    public function massDestroy(MassDestroyUserRequest $request)
    {
        User::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
