<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyRapportRequest;
use App\Http\Requests\StoreRapportRequest;
use App\Http\Requests\UpdateRapportRequest;
use App\Rapport;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RapportController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('rapport_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $rapports = Rapport::all();

        return view('admin.rapports.index', compact('rapports'));
    }

    public function create()
    {
        abort_if(Gate::denies('rapport_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.rapports.create');
    }

    public function store(StoreRapportRequest $request)
    {
        $rapport = Rapport::create($request->all());

        if ($request->input('file', false)) {
            $rapport->addMedia(storage_path('tmp/uploads/' . $request->input('file')))->toMediaCollection('file');
        }

        return redirect()->route('admin.rapports.index');
    }

    public function edit(Rapport $rapport)
    {
        abort_if(Gate::denies('rapport_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.rapports.edit', compact('rapport'));
    }

    public function update(UpdateRapportRequest $request, Rapport $rapport)
    {
        $rapport->update($request->all());

        if ($request->input('file', false)) {
            if (!$rapport->file || $request->input('file') !== $rapport->file->file_name) {
                $rapport->addMedia(storage_path('tmp/uploads/' . $request->input('file')))->toMediaCollection('file');
            }
        } elseif ($rapport->file) {
            $rapport->file->delete();
        }

        return redirect()->route('admin.rapports.index');
    }

    public function show(Rapport $rapport)
    {
        abort_if(Gate::denies('rapport_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.rapports.show', compact('rapport'));
    }

    public function destroy(Rapport $rapport)
    {
        abort_if(Gate::denies('rapport_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $rapport->delete();

        return back();
    }

    public function massDestroy(MassDestroyRapportRequest $request)
    {
        Rapport::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
