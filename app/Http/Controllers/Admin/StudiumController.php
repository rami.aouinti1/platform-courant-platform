<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyStudiumRequest;
use App\Http\Requests\StoreStudiumRequest;
use App\Http\Requests\UpdateStudiumRequest;
use App\Studium;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class StudiumController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('studium_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $studia = Studium::all();

        return view('admin.studia.index', compact('studia'));
    }

    public function create()
    {
        abort_if(Gate::denies('studium_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.studia.create');
    }

    public function store(StoreStudiumRequest $request)
    {
        $studium = Studium::create($request->all());

        return redirect()->route('admin.studia.index');
    }

    public function edit(Studium $studium)
    {
        abort_if(Gate::denies('studium_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.studia.edit', compact('studium'));
    }

    public function update(UpdateStudiumRequest $request, Studium $studium)
    {
        $studium->update($request->all());

        return redirect()->route('admin.studia.index');
    }

    public function show(Studium $studium)
    {
        abort_if(Gate::denies('studium_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.studia.show', compact('studium'));
    }

    public function destroy(Studium $studium)
    {
        abort_if(Gate::denies('studium_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $studium->delete();

        return back();
    }

    public function massDestroy(MassDestroyStudiumRequest $request)
    {
        Studium::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
