<?php

Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'Api\V1\Admin', 'middleware' => ['auth:api']], function () {
    // Permissions
    Route::apiResource('permissions', 'PermissionsApiController');

    // Roles
    Route::apiResource('roles', 'RolesApiController');

    // Users
    Route::apiResource('users', 'UsersApiController');

    // Task Statuses
    //Route::apiResource('task-statuses', 'TaskStatusApiController');

    // Task Tags
    Route::apiResource('task-tags', 'TaskTagApiController');

    // Tasks
    Route::post('tasks/media', 'TaskApiController@storeMedia')->name('tasks.storeMedia');
    Route::apiResource('tasks', 'TaskApiController');

    // Tasks Calendars
    //Route::apiResource('tasks-calendars', 'TasksCalendarApiController', ['except' => ['store', 'show', 'update', 'destroy']]);

    // Profiles
    Route::post('profiles/media', 'ProfilesApiController@storeMedia')->name('profiles.storeMedia');
    Route::apiResource('profiles', 'ProfilesApiController');

    // Rapports
    Route::post('rapports/media', 'RapportApiController@storeMedia')->name('rapports.storeMedia');
    Route::apiResource('rapports', 'RapportApiController');

    // Rapport Financiers
    Route::post('rapport-financiers/media', 'RapportFinancierApiController@storeMedia')->name('rapport-financiers.storeMedia');
    Route::apiResource('rapport-financiers', 'RapportFinancierApiController');
});
