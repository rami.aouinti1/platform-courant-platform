<aside class="main-sidebar sidebar-dark-primary elevation-4" style="min-height: 917px;">
    <!-- Brand Logo -->
      <a href="{{ route("admin.dashboard") }}" class="brand-link">
        <img src={{URL::asset('logo.jpg')}} alt="AdminLTE" class="brand-image img-circle"
             style="opacity: .8">
        <span class="brand-text font-weight-light">{{ trans('panel.site_title') }}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->

        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                @if(Auth::user()->photo)
                    <a href="{{ Auth::user()->photo->getUrl() }}" target="_blank">
                        <img src={{ Auth::user()->photo->getUrl('thumb') }} class="img-circle" alt="User Image">
                    </a>
                @else
                    <img src={{URL::asset('logo.jpg')}} class="img-circle" alt="User Image">
                @endif
            </div>
            <div class="info">
                <a href="{{ route("admin.profiles.profile") }}" class="d-block">{{Auth::user()->firstname}}</a>
            </div>
        </div>


        <!-- Sidebar Menu -->
        <nav class="mt-2">


            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">



                <!-- Home -->

                <li class="nav-item">
                    <a href="{{ route("admin.home") }}" class="nav-link">
                        <p>
                            <i class="fas fa-fw fa-tachometer-alt">

                            </i>
                            <span>Chat</span>
                        </p>
                    </a>
                </li>




                <!-- Bureau regional -->

                @can('user_management_access')
                    <li class="nav-item has-treeview {{ request()->is('admin/permissions*') ? 'menu-open' : '' }} {{ request()->is('admin/roles*') ? 'menu-open' : '' }} {{ request()->is('admin/users*') ? 'menu-open' : '' }} {{ request()->is('admin/profiles*') ? 'menu-open' : '' }}">
                        <a class="nav-link nav-dropdown-toggle" href="#">
                            <i class="fa-fw fas fa-users">

                            </i>
                            <p>
                                <span>Bureau Regional</span>
                                <i class="right fa fa-fw fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route("admin.home") }}" class="nav-link">
                                    <p>
                                        <i class="fas fa-fw fa-tachometer-alt">

                                        </i>
                                        <span>{{ trans('global.dashboard') }}</span>
                                    </p>
                                </a>
                            </li>
                            <!-- Members -->
                            <li class="nav-item">
                                <a href="{{ route("admin.users.members") }}" class="nav-link">
                                    <i class="fa-fw fas fa-user"></i> <p> <span>{{ trans('cruds.user.title') }}</span> </p>
                                </a>
                            </li>

                            @can('tasks_calendar_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.tasks-events.index") }}" class="nav-link">
                                        <i class="fa-fw fas fa-calendar"></i><p><span>Events</span></p>
                                    </a>
                                </li>
                            @endcan
                            @can('rapport_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.rapports.index") }}" class="nav-link {{ request()->is('admin/rapports') || request()->is('admin/rapports/*') ? 'active' : '' }}">
                                        <i class="fa-fw fas fa-file">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.rapport.title') }}</span>
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('rapport_financier_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.rapport-financiers.index") }}" class="nav-link {{ request()->is('admin/rapport-financiers') || request()->is('admin/rapport-financiers/*') ? 'active' : '' }}">
                                        <i class="fa-fw fas fa-file-archive">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.rapportFinancier.title') }}</span>
                                        </p>
                                    </a>
                                </li>
                            @endcan
                        <!-- Gallery -->
                            <li class="nav-item">
                                <a href="{{ route("admin.gallery.index") }}" class="nav-link">
                                    <i class="fa-fw fa fa-images"></i> <span>Gallery</span>
                                </a>
                            </li>

                            <!-- Quiz -->
                            <li class="nav-item">
                                <a href="{{ route("admin.tests.index") }}" class="nav-link">
                                    <i class="fa-fw fa fa-question"></i> <span>Quiz</span>
                                </a>
                            </li>

                            <!-- Results -->
                            <li class="nav-item">
                                <a href="{{ route("admin.results.index") }}" class="nav-link">
                                    <i class="fas fa-grin-beam"></i> <span>Results</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                @endcan


               <!-- Message -->

                <!-- Administration -->


                    <li class="nav-item has-treeview {{ request()->is('admin/contact-companies*') ? 'menu-open' : '' }} {{ request()->is('admin/contact-contacts*') ? 'menu-open' : '' }}">
                        <a class="nav-link nav-dropdown-toggle" href="#">
                            <i class="fa-fw fas fa-phone-square">

                            </i>
                            <p>
                                <span>Comites</span>
                                <i class="right fa fa-fw fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">

                                <li class="nav-item">
                                    <a href="{{ route("admin.contact-companies.index") }}" class="nav-link {{ request()->is('admin/contact-companies') || request()->is('admin/contact-companies/*') ? 'active' : '' }}">
                                        <i class="fa-fw fas fa-building">

                                        </i>
                                        <p>
                                            <span>Liste</span>
                                        </p>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="{{ route("admin.contact-contacts.index") }}" class="nav-link {{ request()->is('admin/contact-contacts') || request()->is('admin/contact-contacts/*') ? 'active' : '' }}">
                                        <i class="fa-fw fas fa-user-plus">

                                        </i>
                                        <p>
                                            <span>Contact</span>
                                        </p>
                                    </a>
                                </li>

                        </ul>
                    </li>


                    <li class="nav-item has-treeview {{ request()->is('admin/crm-statuses*') ? 'menu-open' : '' }} {{ request()->is('admin/crm-customers*') ? 'menu-open' : '' }} {{ request()->is('admin/crm-notes*') ? 'menu-open' : '' }} {{ request()->is('admin/crm-documents*') ? 'menu-open' : '' }}">
                        <a class="nav-link nav-dropdown-toggle" href="#">
                            <i class="fa-fw fas fa-briefcase">

                            </i>
                            <p>
                                <span>Channel</span>
                                <i class="right fa fa-fw fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">

                                <li class="nav-item">
                                    <a href="{{ route("admin.crm-statuses.index") }}" class="nav-link {{ request()->is('admin/crm-statuses') || request()->is('admin/crm-statuses/*') ? 'active' : '' }}">
                                        <i class="fa-fw fas fa-folder">

                                        </i>
                                        <p>
                                            <span>Status</span>
                                        </p>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="{{ route("admin.crm-customers.index") }}" class="nav-link {{ request()->is('admin/crm-customers') || request()->is('admin/crm-customers/*') ? 'active' : '' }}">
                                        <i class="fa-fw fas fa-user-plus">

                                        </i>
                                        <p>
                                            <span>Members</span>
                                        </p>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="{{ route("admin.crm-notes.index") }}" class="nav-link {{ request()->is('admin/crm-notes') || request()->is('admin/crm-notes/*') ? 'active' : '' }}">
                                        <i class="fa-fw fas fa-sticky-note">

                                        </i>
                                        <p>
                                            <span>Note</span>
                                        </p>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="{{ route("admin.crm-documents.index") }}" class="nav-link {{ request()->is('admin/crm-documents') || request()->is('admin/crm-documents/*') ? 'active' : '' }}">
                                        <i class="fa-fw fas fa-folder">

                                        </i>
                                        <p>
                                            <span>Document</span>
                                        </p>
                                    </a>
                                </li>

                        </ul>
                    </li>


                @php($unread = \App\QaTopic::unreadCount())
                <li class="nav-item">
                    <a href="{{ route("admin.messenger.index") }}" class="nav-link"><i class="fa-fw fa fa-envelope"></i><span>{{ trans('global.messages') }}</span>
                        @if($unread > 0)
                            <span class="badge badge-warning right">
                                <strong> {{ $unread }} </strong>
                            </span>
                        @endif
                    </a>
                </li>

                <!-- Documentation -->

                <li class="nav-item">
                    <a href="{{ route("admin.documentation.index") }}" class="nav-link"><i class="fa-fw fa fa-book"></i><span>Documentation</span>
                    </a>
                </li>


                <!-- Logout -->

                <li class="nav-item">
                    <a href="#" class="nav-link" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                            <p>
                                <i class="fas fa-fw fa-sign-out-alt">

                                </i>
                                <span>{{ trans('global.logout') }}</span>
                            </p>
                        </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
