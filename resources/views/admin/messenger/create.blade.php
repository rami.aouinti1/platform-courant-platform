@extends('admin.messenger.template')

@section('title', trans('global.new_message'))

@section('messenger-content')
    <div class="card card-warning card-outline">
        <div class="card-header">
            <h3 class="card-title">Compose New Message</h3>
        </div>
        <!-- /.card-header -->
        <form action="{{ route("admin.messenger.storeTopic") }}" method="POST">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="recipient" class="control-label">
                        {{ trans('global.recipient') }}
                    </label>
                    <select name="recipient" class="form-control">
                        @foreach($users as $user)
                            <option value="{{ $user->id }}">{{ $user->email }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="subject" class="control-label">
                        {{ trans('global.subject') }}
                    </label>
                    <input type="text" name="subject" class="form-control" />
                </div>


                <div class="form-group">
                    <label for="content" class="control-label">
                        {{ trans('global.content') }}
                    </label>
                    <textarea name="content" class="form-control" style="height: 300px"></textarea>
                </div>



                <div class="form-group">
                <div class="btn btn-default btn-file">
                    <i class="fas fa-paperclip"></i> Attachment
                    <input type="file" name="attachment">
                </div>
                <p class="help-block">Max. 32MB</p>
            </div>


        </div>
        <!-- /.card-body -->

            <div class="card-footer">
            <div class="float-right">
                <button type="button" class="btn btn-default"><i class="fas fa-pencil-alt"></i> Draft</button>
                <input type="submit" value="{{ trans('global.submit') }}" class="btn btn-warning" />
            </div>
            <button type="reset" class="btn btn-default"><i class="fas fa-times"></i> Discard</button>
        </div>

        </form>
        <!-- /.card-footer -->
    </div>

    <script>
        $(function () {
            //Add text editor
            $('#compose-textarea').summernote()
        })
    </script>
@stop
