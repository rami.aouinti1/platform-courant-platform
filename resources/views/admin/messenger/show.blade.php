@extends('admin.messenger.template')

@section('title', 'Read Mail')

@section('messenger-content')
    <div class="card card-warning card-outline">

        <div class="card-header">
            <h3 class="card-title">Read Mail</h3>

            <div class="card-tools">
                <a href="#" class="btn btn-tool" data-toggle="tooltip" title="Previous"><i class="fas fa-chevron-left"></i></a>
                <a href="#" class="btn btn-tool" data-toggle="tooltip" title="Next"><i class="fas fa-chevron-right"></i></a>
            </div>
        </div>
        <!-- /.card-header -->

        <div class="card-body p-0">
            <div class="mailbox-read-info">
                <h5> {{ $topic->subject }}</h5>
                <h6>From: {{ $topic->email }}
                    <span class="mailbox-read-time float-right">{{ $topic->created_at->diffForHumans() }}</span></h6>
            </div>
            <!-- /.mailbox-read-info -->
        @foreach($topic->messages as $message)
            <!-- /.mailbox-controls -->
            <div class="mailbox-read-message">
                <div class="row">
                    {{ $message->sender->name }} :
                        {{ $message->content }}
                </div>
            </div>
        @endforeach
            <!-- /.mailbox-read-message -->
        </div>

        <!-- /.card-body -->

        <div class="card-footer bg-white">
            <ul class="mailbox-attachments d-flex align-items-stretch clearfix">
                <li>
                    <span class="mailbox-attachment-icon"><i class="far fa-file-pdf"></i></span>

                    <div class="mailbox-attachment-info">
                        <a href="#" class="mailbox-attachment-name"><i class="fas fa-paperclip"></i> Sep2014-report.pdf</a>
                        <span class="mailbox-attachment-size clearfix mt-1">
                          <span>1,245 KB</span>
                          <a href="#" class="btn btn-default btn-sm float-right"><i class="fas fa-cloud-download-alt"></i></a>
                        </span>
                    </div>
                </li>
                <li>
                    <span class="mailbox-attachment-icon"><i class="far fa-file-word"></i></span>

                    <div class="mailbox-attachment-info">
                        <a href="#" class="mailbox-attachment-name"><i class="fas fa-paperclip"></i> App Description.docx</a>
                        <span class="mailbox-attachment-size clearfix mt-1">
                          <span>1,245 KB</span>
                          <a href="#" class="btn btn-default btn-sm float-right"><i class="fas fa-cloud-download-alt"></i></a>
                        </span>
                    </div>
                </li>
                <li>
                    <span class="mailbox-attachment-icon has-img"><img src="../../dist/img/photo1.png" alt="Attachment"></span>

                    <div class="mailbox-attachment-info">
                        <a href="#" class="mailbox-attachment-name"><i class="fas fa-camera"></i> photo1.png</a>
                        <span class="mailbox-attachment-size clearfix mt-1">
                          <span>2.67 MB</span>
                          <a href="#" class="btn btn-default btn-sm float-right"><i class="fas fa-cloud-download-alt"></i></a>
                        </span>
                    </div>
                </li>
                <li>
                    <span class="mailbox-attachment-icon has-img"><img src="../../dist/img/photo2.png" alt="Attachment"></span>

                    <div class="mailbox-attachment-info">
                        <a href="#" class="mailbox-attachment-name"><i class="fas fa-camera"></i> photo2.png</a>
                        <span class="mailbox-attachment-size clearfix mt-1">
                          <span>1.9 MB</span>
                          <a href="#" class="btn btn-default btn-sm float-right"><i class="fas fa-cloud-download-alt"></i></a>
                        </span>
                    </div>
                </li>
            </ul>
        </div>
        <!-- /.card-footer -->
        <div class="card-footer">
            <div class="float-right">
                @if($topic->receiverOrCreator() !== null && !$topic->receiverOrCreator()->trashed())
                    <a href="{{ route('admin.messenger.reply', [$topic->id]) }}" class="btn btn-warning">
                        {{ trans('global.reply') }}
                    </a>
                @endif
                <button type="button" class="btn btn-default"><i class="fas fa-share"></i> Forward</button>
            </div>
            <button type="button" class="btn btn-default"><i class="far fa-trash-alt"></i> Delete</button>
            <button type="button" class="btn btn-default"><i class="fas fa-print"></i> Print</button>
        </div>

        <!-- /.card-footer -->
    </div>
@endsection
