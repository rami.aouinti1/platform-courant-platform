@extends('layouts.admin')

@section('content')
<div class="content">
    <div class="row">
        <p class="col-lg-12">
            @yield('title')
        </p>
    </div>
    <div class="row">
        <div class="col-lg-3">
            <p>
                <a href="{{ route('admin.messenger.createTopic') }}" class="btn btn-warning btn-block mb-3">
                    {{ trans('global.new_message') }}
                </a>
            </p>
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Inbox</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body p-0">
                    <ul class="nav nav-pills flex-column">
                        <li class="nav-item active">
                            <a href="{{ route('admin.messenger.index') }}" class="nav-link">
                                <i class="fas fa-inbox"></i>  {{ trans('global.all_messages') }}
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin.messenger.showInbox') }}" class="nav-link">
                                <i class="fas fa-inbox"></i>
                                @if($unreads['inbox'] > 0)
                                    <strong>
                                        {{ trans('global.inbox') }}
                                        ({{ $unreads['inbox'] }})
                                    </strong>
                                @else
                                    {{ trans('global.inbox') }}
                                @endif
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin.messenger.showOutbox') }}" class="nav-link">
                                <i class="far fa-envelope"></i>
                                @if($unreads['outbox'] > 0)
                                    <strong>
                                        {{ trans('global.outbox') }}
                                        ({{ $unreads['outbox'] }})
                                    </strong>
                                @else
                                    {{ trans('global.outbox') }}
                                @endif
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="far fa-file-alt"></i> Drafts
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="fas fa-filter"></i> Junk
                                <span class="badge bg-warning float-right">65</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="far fa-trash-alt"></i> Trash
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Labels</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body p-0">
                    <ul class="nav nav-pills flex-column">
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="far fa-circle text-danger"></i>
                                Important
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="far fa-circle text-warning"></i> Promotions
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="far fa-circle text-primary"></i>
                                Social
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- /.card-body -->
            </div>


        </div>
        <div class="col-lg-9">
            @yield('messenger-content')
        </div>
    </div>
</div>
<script>
    $(function () {
        //Enable check and uncheck all functionality
        $('.checkbox-toggle').click(function () {
            var clicks = $(this).data('clicks')
            if (clicks) {
                //Uncheck all checkboxes
                $('.mailbox-messages input[type=\'checkbox\']').prop('checked', false)
                $('.checkbox-toggle .far.fa-check-square').removeClass('fa-check-square').addClass('fa-square')
            } else {
                //Check all checkboxes
                $('.mailbox-messages input[type=\'checkbox\']').prop('checked', true)
                $('.checkbox-toggle .far.fa-square').removeClass('fa-square').addClass('fa-check-square')
            }
            $(this).data('clicks', !clicks)
        })

        //Handle starring for glyphicon and font awesome
        $('.mailbox-star').click(function (e) {
            e.preventDefault()
            //detect type
            var $this = $(this).find('a > i')
            var glyph = $this.hasClass('glyphicon')
            var fa    = $this.hasClass('fa')

            //Switch states
            if (glyph) {
                $this.toggleClass('glyphicon-star')
                $this.toggleClass('glyphicon-star-empty')
            }

            if (fa) {
                $this.toggleClass('fa-star')
                $this.toggleClass('fa-star-o')
            }
        })
    })
</script>
@stop
