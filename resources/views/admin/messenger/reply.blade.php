@extends('admin.messenger.template')

@section('title', trans('global.reply'))

@section('messenger-content')

    <div class="card card-warning card-outline">
        <div class="card-header">
            <h3 class="card-title">Compose New Message</h3>
        </div>
<div class="row">
    <div class="col-lg-12">
        <form action="{{ route("admin.messenger.reply", [$topic->id]) }}" method="POST">
            @csrf
            <div class="card card-default">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12 form-group">
                            <label for="content" class="control-label">
                                {{ trans('global.content') }}
                            </label>
                            <textarea name="content" class="form-control"></textarea>
                        </div>
                    </div>
                    <input type="submit" value="{{ trans('global.reply') }}" class="btn btn-warning" />
                </div>
            </div>
        </form>
    </div>
</div>
    </div>
@stop
