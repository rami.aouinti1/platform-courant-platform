@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="card card-warning card-outline">
            <div class="card-header">
               <h3 class="card-title">
                        <img src={{URL::asset('tunisie.png')}}  alt="User" width="50" height="50">
                    </h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-7 col-sm-9">
                        <div class="tab-content" id="vert-tabs-right-tabContent">
                            <div class="tab-pane fade show active" id="vert-tabs-right-home" role="tabpanel" aria-labelledby="vert-tabs-right-home-tab">
                                تونس، رسميًا الجمهورية التونسية، هي دولة تقع في شمال أفريقيا يحدها من الشمال والشرق البحر الأبيض المتوسط ومن الجنوب الشرقي ليبيا (459 كم) ومن الغرب الجزائر (965 كم). عاصمتها مدينة تونس. تبلغ مساحة الجمهورية التونسية 163,610 كم2. يبلغ سكان الجمهورية التونسية حسب آخر الإحصائيات سنة 2014 ما يقارب 10 ملايين و982,8 ألف نسمة.
                                <br>
                                <br>
                                لعبت تونس أدوارا هامة في التاريخ القديم منذ عهد الفينيقيين والأمازيغ والقرطاجيين والونداليين والرومان وقد عرفت باسم مقاطعة أفريكا إبان الحكم الروماني لها والتي سميت باسمها كامل القارة. فتحها المسلمون  في القرن السابع الميلادي وأسسوا فيها مدينة القيروان سنة 50 هـ لتكون ثاني مدينة إسلامية في شمال أفريقيا بعد الفسطاط. في ظل الدولة العثمانية، كانت تسمى "الإيالة التونسية". وقعت تحت الاحتلال الفرنسي في عام 1881، ثم حصلت على استقلالها في عام 1956 لتصبح رسميا المملكة التونسية في نهاية عهد محمد الأمين باي. مع إعلان الجمهورية التونسية في 25 يوليو 1957، أصبح الحبيب بورقيبة أول رئيس لها.
                                <br>
                                <br>
                                تلى الأخيرَ في رئاسة الجمهورية زين العابدين بن علي بالانقلاب عام 1987، واستمر حكمه حتى 2011 حين هرب خلال الثورة التونسية. اعتمدت تونس على الصناعات الموجهة نحو التصدير في عملية تحرير وخصخصة الاقتصاد الذي بلغ متوسط نمو الناتج المحلي الإجمالي 5 ٪ منذ أوائل 1990، ويذكر ان تونس عانت الفساد في ظل حكم الرئيس السابق.
                                تونس لديها علاقات وثيقة وتاريخية مع كل من الولايات المتحدة الأمريكية والاتحاد الأوروبي وهي حليف رئيسي خارج الناتو ولديها عدة اتفاقيات شراكة متقدمة تجمعها مع الاتحاد الأوروبي والذي يعد الزبون الأول لتونس والحليف الاقتصادي القوي. تونس هي أيضا عضو في جامعة الدول العربية والاتحاد الأفريقي. وأنشأت تونس علاقات وثيقة مع فرنسا على وجه الخصوص، من خلال التعاون الاقتصادي والتحديث الصناعي، وبرامج الخصخصة. وقد جعلت النهج الذي تتبعه الحكومة في الصراع بين إسرائيل وفلسطين كما أنها وسيط في مجال الدبلوماسية في الشرق الأوسط ومساهم كبير في فرض السلام في العالم عبر قواتها المنتشرة في مناطق النزاع والتابعة للأمم المتحدة .
                            </div>
                            <div class="tab-pane fade" id="vert-tabs-right-profile" role="tabpanel" aria-labelledby="vert-tabs-right-profile-tab">
                                Mauris tincidunt mi at erat gravida, eget tristique urna bibendum. Mauris pharetra purus ut ligula tempor, et vulputate metus facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas sollicitudin, nisi a luctus interdum, nisl ligula placerat mi, quis posuere purus ligula eu lectus. Donec nunc tellus, elementum sit amet ultricies at, posuere nec nunc. Nunc euismod pellentesque diam.
                            </div>
                        </div>
                    </div>
                    <div class="col-5 col-sm-3">
                        <div class="nav flex-column nav-tabs nav-tabs-right h-100" id="vert-tabs-right-tab" role="tablist" aria-orientation="vertical">
                            <a class="nav-link active" id="vert-tabs-right-home-tab" data-toggle="pill" href="#vert-tabs-right-home" role="tab" aria-controls="vert-tabs-right-home" aria-selected="true">تعريف</a>
                            <a class="nav-link" id="vert-tabs-right-profile-tab" data-toggle="pill" href="#vert-tabs-right-profile" role="tab" aria-controls="vert-tabs-right-profile" aria-selected="false">الدستور</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card -->
        </div>
    </div>
</div>



<div class="container">
    <div class="row justify-content-center">
        <div class="card card-warning card-outline">
            <div class="card-header">
               <h3 class="card-title">
                        <img src={{URL::asset('logo.jpg')}}  alt="User" width="50" height="50">
               </h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-7 col-sm-9">
                        <div class="tab-content" id="vert-tabs-right-tabContent">
                            <div class="tab-pane fade show active" id="vert-tabs-right-home-courant" role="tabpanel" aria-labelledby="vert-tabs-right-home-tab">
                                لقد رزحت بلادنا عقودا طويلة تحت نيْر الاستبداد، حيث عانى فيها الشعب التّونسي مآسي القهر الاستعماري، وظلم نظامين استبداديّيْن تعاقبا على حكم الشّعب بعد الاستقلال. ورغم المكاسب التي تحققّت في بناء الدّولة الحديثة بشأن حقوق المرأة والتّعليم والصّحة والإدارة والبنية الأساسيّة بفضل إرادة الشّعب في التحرّر والرقيّ بقيادة نخب البلاد، فإنّ النّظام السياسي للدّولة “الوطنيّة” سرعان ما تحوّل إلى نظام استبدادي قائم على الحزب الواحد وعلى سلطة الرّئيس الحاكم مدى الحياة. فتولّد عنه نظام دكتاتوري قهري انتهك الحقوق والحريّات، وتماهى فيه الحزب مع الدّولة، ووُظّف فيه الإعلام للدعاية والتضليل، والجهاز الأمني للقمع والظلم، و إمكانيات الدولة لخدمة الفاسدين وحمايتهم .
                                <br>
                                <br>
                                ولئن قدّمت أجيال متتالية من المناضلين السياسيين والحقوقيين والنقابيين – بمختلف مرجعيّاتهم الفكرية ورؤاهم الإيديولوجية – تضحيات خالدة في سبيل التحرّر من الاستبداد ومن أجل ضمان حياة سياسيّة ديمقراطيّة، فإنّ النظام الدكتاتوري قد تمكّن من بسط هيمنته. فتحكّم في الدولة والمجتمع بالترهيب والقهر، وارتبط فيه الاستبداد السياسي بالفساد الإداري والاقتصادي، حتّى أصبحا متلازمين، ومؤذنين بسقوط النّظام .
                                <br>
                                <br>
                                وكانت أحداث 17 ديسمبر 2010 الشّرارة التي أوقدت نار ثورة التونسيّين والتونسيّات على نظام الاستبداد الذي انتهك حقوقهم وحريّاتهم، وأذلّهم في وقت كانوا يشاهدون ما تنعم به الأمم المتقدّمة من حقوق وحريات، وما تحقّقه من رفاه اجتماعي، وازدهار اقتصادي، وتقدّم علمي. وفي وقت كان فيه النّظام يُسوّق لصورة برّاقة مزيّفة ” للمعجزة التونسيّة ” غير مطابقة لحقيقة ما يعيشه الشعب التونسي من ظلم وقهر وتهميش وتفاوت جهوي.
                                <br>
                                <br>
                                وما كان لانتفاضة أهلنا في سيدي بوزيد أن تنتشر في كامل البلاد لو لم يعم البلاد الإحساس بالقهر والظلم، والافتقار لأبسط الحقوق والحريات الضّامنة للكرامة الإنسانية. وقد سبقت الثورة بسنتين انتفاضة أهلنا بالحوض المنجمي، وببعض الأشهر انتفاضة أهلنا ببن قردان . وقد كان تحدّي أبناء الشعب الشّجعان لآلة القمع، وتضحيتهم بأنفسهم، وعزمهم إنهاء الاستكانة، وتحرّرهم من الخوف، كلّها دوافعَ للتضامن، وشحذا للهمم، ونضالا امتدّ إلى كامل البلاد، فأطاح بالنظام، وانتهى بهروب الطاغية.
                                <br>
                                <br>
                                لقد مثّل يوم 14 جانفي 2011 بداية تاريخ جديد للشعب التّونسي تحرّر فيه من الظّلم والقهر والاستبداد، و فرض– في مسار ثوري شاق – على الحكومتين المتعاقبتين في الفترة الانتقالية الأولى حلّ الحزب الذي حكم البلاد وقهر العباد، وإقرار قانون العفو التشريعي العام. فتحررت الكلمة، وضُمِن حق التنظّم الحزبي والجمعيّاتي، وانطلق الجميع في بناء مشهد سياسي جديد قائم على التنافس والتجاذب في اتّجاه التأسيس لشرعية سياسية تأسيسيّة أصليّة منها تنبثق جميع الهياكل والمؤسّسات الشرعية للدّولة.
                                <br>
                                <br>
                                وقد كان يوم 23 أكتوبر 2011 لحظة فارقة في التاريخ السّياسي المعاصر لتونس ، تاريخ أوّل انتخابات حرة وشفافة، اختار فيها الشّعب ممثّليه في المجلس الوطني التأسيسيّ للاضطلاع بمهمّة كتابة دستور جديد، وانتخاب حكومة شرعية، ورئيس للجمهورية لفترة مؤقّتة ثانية. إلاّ أنّ الاعتقاد بأنّ ما أفرزته الانتخابات من هياكل ومؤسّسات شرعية كفيل باستكمال مسار الثّورة، وبإنهاء مرحلة الاستبداد، هو من قبيل الوهم بأنّ هروب الطاغية كفيل باقتلاع جذور نظام قائم على الاستبداد والفساد.
                                <br>
                                <br>
                                إنّ التّحدي الرّئيسي ــ اليوم ــ يتمثّل في وضع الآليات والضمانات اللاّزمة لعدم عودة الاستبداد والفساد، والقطع مع سياسة التّهميش والتمييز بين المواطنين، وبين الجهات، لا سيما وأنّ وضع البلاد الأمني والاقتصادي من جهة، وطبيعة الصّراع في المشهد السياسي والاجتماعي من جهة أخرى، لا يبعدان، بأي حال، شبح الانتكاس. وإذا سلّمنا بأنّ ضعف الحكومة، وتردّدها كان فرصة لبعض الأفراد والجماعات حتّى يعيدوا تنظيم شبكات الفساد، ويخرقوا القوانين، وينتهكوا هيبة مؤسسات الدولة، ويجهضوا أهداف الثورة. فلا بدّ من الإقرار بأنّ هذا الوهن قد أدّى إلى تعقيد الوضع الأمني، واحتدام الصّراع السياسي، وتأزم الوضع الاقتصادي والاجتماعي.
                                <br>
                                <br>
                                ورغم وجود أشخاص من أصحاب العزائم الصادقة، في مختلف شرائح المجتمع والمؤسسات والأحزاب، سواء تلك التي في السلطة أو في المعارضة، فإنّ منظومة الفساد، و إعادة إنتاج النظام السّابق أصبحت الآن تعمل بنسق حثيث لاسترجاع مكانتها بنفس الوجوه القديمة، أو بجدد تراءى لهم أنّ الوصول إلى الحكم، أو البقاء فيه يقتضي القبول بالأساليب التقليدية في إدارة الشأن العام، بل ودعمها في بعض الحالات لكسب الأنصار.
                                <br>
                                <br>
                                و في خضم هذا الواقع السياسي الجديد الذي يعيشه بلدنا اضطلعت بعض الأحزاب السياسية بدورها النقدي. إلاّ أنّ بعضها ــ وفي إطار حرب المواقع و الصراع الإيديولوجي ــ قد زاد الوضع تأزّما بتوظيف المطالب الاجتماعية – المشروعة والواقعية- في التحريض على من يحكم دون أن توجّه جهدها إلى تقديم البدائل، والتركيز على مواطن الخلل المتمثلة في تردّد الحكومة، وعدم فرضها لسلطة القانون على الجميع، وعدم محاربتها للبيروقراطية والفساد، وسكوتها عن أخطاء بعض المسؤولين، وضعف الشفافيّة، وتوظيف أساليب النظام السابق . أمّا بعض الأحزاب الأخرى فقد غرقت في مشاكلها الدّاخلية لغياب التسيير الديمقراطي، واستفحال النزاعات الشخصيّة، وعدم استيعابها لمفهوم المؤسسة، في حين فقد غيرها استقلاليته لارتباطه بمموّلين أصبحوا ضمانة استمراره. وعموما، فإنّ المشهد السياسي أصبح قائما على الاختلاف الإيديولوجي الذي عوض أن يكون عنصر إثراء للفكر السياسي، أصبح مكرّسا للصراع والتطاحن من أجل السلطة، و مفجّرا للحقد والكراهية، ومؤذنا بتفاقم ظاهرة العنف المعنوي والمادي على أساس الاستقطاب الثنائي، أو التّكفير، أو التّخوين.
                                <br>
                                <br>
                                إنّ مشروع تأسيسنا لحزب جديد ــ اليوم ــ يهدف إلى المساهمة في إيجاد البديل للتّونسيين الذين ملّوا الخطاب السياسي السائد لدى أحزاب السّلطة وأحزاب المعارضة، ويريدون لبلدهم أن يقلع نحو آفاق الرقي والتقدم على جميع المستويات. ونحن نعتزم أن يُبعث مشروعنا ويُدار بشكل عصري ديمقراطي شفاف، يضمّ عند تأسيسه، وفي مستوى كل المسؤوليات المركزية والجهوية والمحلية، كفاءات ومناضلين يحركهم حب الوطن، مقتنعين بحاجته إليهم في هذه المرحلة التأسيسيّة الحرجة، مستعدين للتضحية وتحمّل المسؤولية في سبيله، وقادرين على تصوّر البدائل وتقديمها والإقناع بها. لا يعادون من يختلف معهم في الرأي، ولكنهم مقتنعون بالأفكار والآراء الواردة في هذا البيان التأسيسي، وفي وثيقة الهوية والخط السياسي، حيث يتعاهدون على العمل في حدودها واثقين في المستقبل، وفي قدرة الشعب التونسي على النهوض، متحلين بالإرادة اللازمة لتجاوز كل المصاعب التي قد تعترض سبيلهم
                            </div>
                            <div class="tab-pane fade" id="vert-tabs-right-profile-courant" role="tabpanel" aria-labelledby="vert-tabs-right-profile-tab">
                                <strong>الباب الأول: أحكام عامة </strong>
                                <br>
                                <br>
                                <br>
                                <br>
                                الفصل الأول: تكوّن بين الأشخاص الذين اتفقوا على هذا النظام الأساسي أو الذين سيتفقون عليه حزب سياسي أطلق عليه اسم التّيار الدّيمقراطي وهو خاضع للقانون المنظّم للأحزاب ولأحكام هذا النّظام الأساسي ونظامه الدّاخلي.
                                الفصل 2 : مقر الحزب : .شارع اليابان عمارة 45 شقة ب 53 مونبليزير – تونس
                                الفصل 3  : شعار الحزب هو: – وعي – إرادة – إنجاز و رمزه مركب شراعي
                                الفصل4: جميع المسؤوليات الحزبية تمارس عن طريق الانتخاب باستثناء المسؤوليات ذات الطابع التقني. جميع المقررات والمواقف التي تصدر عن هياكل الحزب يجب أن تكون خاضعة لقاعدة الأغلبية وفق ما ينص عليه النظام الداخلي.
                                الباب الثاني: البرنامج العام للحزب و هويته و خطه السياسي
                                الفصل5 : التيّار الدّيمقراطي حزب اجتماعي ديمقراطي، يدعو إلى نظام يقوم على العدالة الاجتماعية والتّوزيع العادل للثروة، ويضمن مجانيّة التّعليم للجميع والتّغطية الصحيّة لكل الفئات، و يكفل المبادرة الخاصّة والملْكيّة الفرديّة والمنافسة الحرّة مع اضطلاع الدّولة بدور تعديلي وبالاستثمار العمومي والمحافظة على الملكية العمومية للقطاعات الحيوية و تأهيل القطاع العام.
                                • حزبنا يدعو لضمان حقوق الإنسان في كونيّتها وشموليّتها ويؤمن بأنّ للحريّات حدودا ضرورية في المجتمعات الديمقراطية توضع لحماية حقوق الغير والأمن العام والصّحة العامّة.
                                • يرى الحزب أنّ الدولة الدّيمقراطية تضمن لمواطنيها ممارسة حقوقهم وحريّاتهم، وتعمل على فرض سلطة القانون على الجميع
                                • يرى الحزب أنّ الدولة الدّيمقراطية تكرّس مبدأ المساواة بين المواطنين بقطع النّظر عن انتمائهم الجهوي أو الدّيني أو العرقي أو النّوعي، وتعمل على دعم مشاركة المرأة في مراكز القرار على أساس الكفاءة
                                • يشجّع الحزب العمل الجمعياتي، ويدعو إلى استقلاله التّام عن السّلطة القائمة، وعن سائر الأحزاب، وعن الصّراع السياسي والانتخابي . ويدعم الحزب منظمات حقوق الإنسان المستقلة التي تدافع عن الإنسان دون تمييز
                                • يدافع الحزب عن استقلال السّلطة القضائية، وعن تحسين وضعية القضاة، ويدعو إلى إقصاء المورطين منهم في الفساد، وفي تنفيذ التعليمات، وكلّ من تهاون في الدفاع عن استقلاليتهم و حيادهم.
                                • يعمل الحزب على أن تولي الدولة العناية اللازمة بالبيئة، و تجعل من حمايتها ومن حسن التصرف في الموارد المنجمية والطاقية غير المتجددة والمياه مسالة ذات أولية في سياساتها التنموية بما يحفظ حقوق الأجيال القادمة.
                                • يعمل الحزب على أن توفّر الدولة أكبر قدر من الإمكانيات المتاحة لتطوير التعليم، ودعم البحث العلمي، وتشجيع الثقافة والإعلام و الرياضة.
                                • تُضبط العلاقة بين الدين والدولة على النحو التالي:
                                –  تقرّ الدّولة الهُويّة العربيّة الإسلاميّة، وتعتبر الدّين الإسلامي دين أغلبيّة التّونسيين.
                                – تضمن الدّولة حريّة المعتقد للجميع ما لم تُخل بالأمن العام.
                                – توفّر الدّولة أماكن العبادة وتشرف عليها.
                                – تعمل الدّولة على تحييد أماكن العبادة عن العمل السّياسي والدّعاية الحزبيّة
                                – تضمن الدّولة تعليم التّربية الدينيّة في المدارس والمعاهد العموميّة
                                • في صورة التّنازع بين مصلحة الدّولة و مصلحة الحزب تعطى الأولوية لمصلحة الدّولة
                                • يلتزم الحزب بسياسة المصارحة و الشّفافية مع منخرطيه و سائر المواطنين، و يتعامل معهم على أساس كونهم مسؤولين
                                • يعمل الحزب على إحداث تغيير في بعض الظواهر الثقافية السلبية للتونسيين دون تدخل في الحياة الخاصة للناس و نمط حياتهم و اختياراتهم الشخصية ، و يواجه الناس بالحقيقة قدر الإمكان حتّى وإن اقتضى الأمر خسارة أصواتهم
                                • يعتبر الحزب مكافحة الفساد من أولويات الدولة في كل المؤسسات، ويعمل على استئصال هذه الظاهرة أو التقليص منها إلى أدنى حد، ولا يتسامح مع الفساد و خاصة عندما يتورط فيه المنخرطون فيه أو المتحملون للمسؤولية في الدولة
                                • يسعى الحزب إلى أن يكون حزبا قويا يصل إلى السلطة و ذلك بتحسين إدارته و هيكلته و تسييره بشكل ديمقراطي وباستقطابه للكفاءات و لعدد كبير من المنخرطين النوعيين يشترط فيهم حسن السلوك و الحس الوطني . و في صورة عدم الوصول إلى السلطة يضطلع الحزب بدور المعارضة الفعّالة و المسؤولة التي تنقد و تقدم البدائل دون أن تصل إلى سياسة العرقلة أو الإرباك ما دامت الحكومة ملتزمة بالقوانين .
                                • يمكن للحزب الدخول في تحالفات أو التنسيق مع أحزاب أخرى غير مورّطة مع نظام الاستبداد، و غير متورّطة في الفساد، ولا تتبنى أفكارا مخالفة لقيم الجمهورية .
                                • حزبنا غير معاد للإيديولوجيات و الأفكار المخالفة التي تتبناها الأحزاب و المنظّمات الأخرى ما دامت ملتزمة بالدّستور والقوانين، وهو يتميّز بالتّسامح و عدم رفض الآخر.
                                • يعمل الحزب من أجل التّهيئة لتحقيق مشروع إقامة دولة فدرالية عربية تجمع بين الشعوب العربية المتحررة من الاستبداد
                                • يعمل الحزب على تكريس سياسة خارجية للدولة تقوم على حسن العلاقات مع سائر الدول، وعدم التدخل في شؤون الغير، مع عدم السكوت عن الانتهاكات البليغة لحقوق الإنسان، وترفض التطبيع مع الكيان الصهيوني، وتساهم في إيجاد حل عادل للقضية الفلسطينية
                                •يعمل الحزب على أن تكون الانتخابات نزيهة و شفافة و معبّرة عن إرادة الشعب وحريته، و يسعى إلى أن تتم في مناخ سلمي و آمن، وفي ظل حياد الإدارة، وألاّ تُستغل الإمكانيات المالية لأي طرف في شراء الأصوات، أو توظيف إمكانيات الدولة لاستمالة المواطنين، و يطالب الحزب بالرقابة المالية على الأحزاب السياسية والجمعيات والمؤسسات الإعلامية .
                                • يعمل الحزب على تكريس أكبر قدر من الشفافية في إدارة الشأن العام وحق المواطن في الحصول على المعلومة .
                                الباب الثالث العضوية
                                الفصل 6:  يعتبر عضوا في الحزب كل شخص يقبل بهوية الحزب و خطه السياسي وبنظامه الأساسي ونظامه الداخلي و يستجيب للشروط الواردة بالنصوص المذكورة  و يمكن ان يشترط النظام الداخلي استيفاء طالب الانخراط لمدة معينة من النشاط قبل قبوله عضوا في الحزب.
                                الفصل 7 : يقع الانخراط في الحزب بناء على طلب ينظر فيه حسب أحكام النظام الداخلي. و كلّ منخرط ملزم بدفع معاليم الاشتراك بانتظام.
                                الفصل 8 يفقد صفة العضوية :
                                – كلّ من قرّر الحزب رفته لمخالفته مبادئ الحزب وقوانينه ومقرّراته أو لتبيّن عدم استيفائه لشروط الانخراط بناء على طلب في ذلك من المكتب السّياسي يوجّه إلى لجنة النّظام و يمكن للمكتب السّياسي اتّخاذ قرار التّجميد المؤقّت حسبما ينصّ عليه النّظام الدّاخلي الذي يمكن أن ينصّ على تخويل هياكل أخرى أو عدد من المنخرطين صلاحيّة الإحالة على لجنة النّظام و يضبط قائمة الأخطاء التأديبيّة.
                                – كل من قرّر المكتب السّياسي رفته لعدم دفعه الاشتراك السّنوي بعد التّنبيه عليه بضرورة الخلاص ومرور أجل يحدّده النّظام الدّاخلي .
                                – كلّ من استقال من الحزب طبق إجراءات ينصّ عليها النّظام الدّاخلي.
                                الباب الرابع المالية
                                الفصل 9 : تتكوّن مداخيل الحزب في حدود ما ينصّ عليه القانون من:
                                – اشتراك المنخرطين فيه و مساهمات متحمّلي المسؤوليّة
                                – المساعدات والهبات و الوصايا طبق مقتضيات القانون الدّاخلي
                                – التّمويل العمومي.
                                الفصل 10 : تحفظ جميع أموال الحزب في حساب جار كما يمسك امين المال حسابات الحزب و يكون مسؤولا تحت إشراف الأمين العام على استيفاء كل الإجراءات المنصوص عليها بالقانون و المتعلقة بحسابات الحزب و تمويله.
                                الباب الخامس الهيكلة.
                                الفصل 11  : يتألّف الحزب من هياكل وطنية وهياكل جهوية ومكاتب محدثة ولجان قارة :
                                القسم الاول الهياكل الوطنية.
                                الفصل 12 : المؤتمر الوطني هو أعلى سلطة في الحزب وهو من : – يحدد سياسة الحزب – يراقب تنفيذ المجلس الوطني والمكتب السياسي والمكتب التنفيذي مقرّراته – ينظر في تنقيح النظام الأساسي والنظام الداخلي للحزب. و يلتئم المؤتمر بصفة عادية وجوبا مرة واحدة كل سنتين ونصف وبصفة استثنائيّة كلّما دعت الحاجة إلى ذلك وفق ما ينّص عليه النّظام الدّاخلي. و يتركب المؤتمر من أعضاء المكتب السّياسي بمن فيهم أعضاء المكتب التّنفيذي وأعضاء المجلس الوطني ومؤسّسي الحزب وممثّلي الهياكل الجهويّة والمكاتب المحدثة والمكاتب بالخارج على قاعدة يتولى ضبطها النظام الداخلي.
                                الفصل 13 : المجلس الوطني هو أعلى سلطة بين مؤتمرين ويقع ضبط عدد أعضائه وطريقة اختيارهم وفق أحكام النظام الداخلي. وهو الذي:
                                – يسهرعلى تنفيذ مقررات المؤتمر الوطني واحترام النظام الأساسي والنظام الداخلي.
                                – يسد الشغورات بالأمانة العامة و بالمكتب السياسي.
                                – يحدد سياسة الحزب في حدود قرارات المؤتمر الوطني.
                                – يطبق قرارات المؤتمر الوطني و لوائحه.
                                – يراقب أعمال المكتب السياسي وتنفيذه لقرارات المؤتمر والمجلس الوطني.
                                الفصل 14 : المكتب السياسي هو الهيأة التنفيذية للحزب، وهو الذي يسهر على تنفيذ قرارات المؤتمر الوطني والمجلس الوطني. يقع انتخاب الأمين العام وأعضاء المكتب السياسي من قبل المؤتمر الوطني يتكون المكتب السياسي من عدد من الأعضاء يضبطه النظام الداخلي ويرأس أشغاله الأمين العام عند حضوره ويكون صوته مرجحا في صورة تساوي الأصوات. الامين العام هو ممثل الحزب ورئيس مكتبه السياسي. يقع توزيع المهام بين أعضاء المكتب السياسي بالتوافق أو بالأغلبية المطلقة حسبما ينص عليه النظام الداخلي ويكون من بينهم وجوبا امين المال. ولا يمكن للامين العام أو لعضو المكتب السياسي تحمل المسؤولية أكثر من دورتين انتخابيتين متتاليتين. يضع الأمين العام بعد استشارة المكتب السياسي تراتيب تضبط التنظيم الإداري و المالي للحزب . كما يضع تراتيب بنفس الشروط لسد الفراغ الناجم عن سكوت النظام الداخلي . و في صورة مخالفة الأمين العام لرأي المكتب السياسي يرفع الامر الى المجلس الوطني للبت فيه.
                                القسم الثاني الهياكل الجهوية
                                الفصل 15 : تتكون الهياكل الجهوية من المكاتب الجهوية و المكاتب المحلية
                                الفصل 16 : يشرف المكتب الجهوي على جملة المكاتب المحلية الموجودة بالجهة وتديره هيأة منتخبة حسبما ينص عليه النظام الداخلي.
                                الفصل 17  : يتكون المكتب المحلي من عدد من المنخرطين في الحزب في منطقة تحدد وفق ما ينص عليه النظام الداخلي ويقع انتخاب هيأته من طرف المنخرطين به. يقع تحديد عدد أعضاء المكتب المحلي بالنظام الداخلي و يمكن إنشاء فروع تابعة للمكتب المحلي يحدد نظامها، النظام الداخلي للحزب.
                                القسم الثالث المكاتب المحدثة واللجان
                                الفصل 18 : ينشىء المجلس الوطني مكاتب تضم المنخرطين في قطاعات معينة كما ينشىء المكاتب بالخارج ويحدد دوائرها.
                                الفصل 19 : يحدد النظام الداخلي اللجان القارة وينظمها.
                                الفصل 20 : ينشىء المكتب السياسي لجانا ويحدد صلاحياتها ويعين أعضاءها .
                                الباب السادس تعليق نشاط الحزب و حله
                                الفصل 21 : لا يمكن تعليق نشاط الحزب مؤقتا او حله إلا بواسطة مؤتمر استثنائي ينعقد للغرض وبأغلبية ثلاثة أرباع المؤتمرين وهو الذي يقرر مصير مكاسب الحزب بهبتها إما الى حزب او الى جمعية او الى الدولة او جماعة محلية.
                                الفصل 22 : في صورة حل التيّار الديمقراطي للاندماج في حزب آخر أو تكوين حزب جديد مع قوى أخرى تكون الأغلبية المطلوبة لاتخاذ القرار بثلثي أعضاء المؤتمر الوطني وتنقل ممتلكات الحزب وجوبا إلى الحزب الجديد.
                                الباب السابع تنقيح النظام الاساسي
                                الفصل 23 : يتم تنقيح النظام الأساسي بأغلبية ثلثي أعضاء المؤتمر الوطني.
                                الباب الثامن أحكام انتقالية
                                الفصل 24 : يتولى المكتب السياسي التأسيسي حال الحصول على التأشيرة القانونية تسيير شؤون الحزب وتركيز هياكله و الاعداد لمؤتمره الاول طبق ما ينص عليه النظام الداخلي.
                                أودع النظام الاساسي للحزب برئاسة الحكومة بتاريخ 9 ماي 2013.

                            </div>
                            <div class="tab-pane fade" id="vert-tabs-right-messages-courant" role="tabpanel" aria-labelledby="vert-tabs-right-messages-tab">
                                Morbi turpis dolor, vulputate vitae felis non, tincidunt congue mauris. Phasellus volutpat augue id mi placerat mollis. Vivamus faucibus eu massa eget condimentum. Fusce nec hendrerit sem, ac tristique nulla. Integer vestibulum orci odio. Cras nec augue ipsum. Suspendisse ut velit condimentum, mattis urna a, malesuada nunc. Curabitur eleifend facilisis velit finibus tristique. Nam vulputate, eros non luctus efficitur, ipsum odio volutpat massa, sit amet sollicitudin est libero sed ipsum. Nulla lacinia, ex vitae gravida fermentum, lectus ipsum gravida arcu, id fermentum metus arcu vel metus. Curabitur eget sem eu risus tincidunt eleifend ac ornare magna.
                            </div>
                            <div class="tab-pane fade" id="vert-tabs-right-settings-courant" role="tabpanel" aria-labelledby="vert-tabs-right-settings-tab">
                                التيّار الدّيمقراطي حزب اجتماعي ديمقراطي، يدعو إلى نظام يقوم على العدالة الاجتماعية والتوزيع العادل للثروة، ويضمن مجانية التعليم للجميع والتغطية الصحية لكل الفئات، و يكفل المبادرة الخاصة والملكية الفردية والمنافسة الحرّة مع اضطلاع الدولة بدور تعديلي وبالاستثمار العمومي والمحافظة على الملكية العمومية للقطاعات الحيوية و تأهيل القطاع العام .
                                <br>
                                <br>
                                •  حزبنا يدعو لضمان حقوق الإنسان في كونيتها وشموليتها ويؤمن بأنّ للحريات حدودا ضرورية في المجتمعات الديمقراطية توضع لحماية حقوق الغير والأمن العام والصحة العامة .
                                <br>
                                <br>
                                •  يرى الحزب أنّ الدولة الدّيمقراطية تضمن لمواطنيها ممارسة حقوقهم وحرياتهم، وتعمل على فرض سلطة القانون على الجميع .
                                <br>
                                <br>
                                •  يرى الحزب أنّ الدولة الديمقراطية تكرس مبدأ المساواة بين المواطنين بقطع النظر عن انتمائهم الجهوي أو الديني أو العرقي أو النوعي، وتعمل على دعم مشاركة المرأة في مراكز القرار على أساس الكفاءة .
                                <br>
                                <br>
                                •  يشجّع الحزب العمل الجمعياتي، ويدعو إلى استقلاله التام عن السّلطة القائمة، وعن سائر الأحزاب، وعن الصراع السياسي والانتخابي . ويدعم الحزب منظمات حقوق الإنسان المستقلة التي تدافع عن الإنسان دون تمييز

                                •  يدافع الحزب عن استقلال السلطة القضائية، وعن تحسين وضعية القضاة، ويدعو إلى إقصاء المورطين منهم في الفساد، وفي تنفيذ التعليمات، وكلّ من تهاون في الدفاع عن استقلاليتهم و حيادهم .
                                <br>
                                <br>
                                •  يعمل الحزب على أن تولي الدولة العناية اللازمة بالبيئة، و تجعل من حمايتها ومن حسن التصرف في الموارد المنجمية والطاقية غير المتجددة والمياه مسالة ذات أولية في سياساتها التنموية بما يحفظ حقوق الأجيال القادمة .
                                <br>
                                <br>
                                •  يعمل الحزب على أن توفّر الدولة أكبر قدر من الإمكانيات المتاحة لتطوير التعليم، و دعم البحث العلمي، وتشجيع الثقافة والإعلام و الرياضة .
                                <br>
                                <br>
                                <br>
                                <br>
                                •  تُضبط العلاقة بين الدين والدولة على النحو التالي :
                                <br>
                                <br>
                                –  تقرّ الدولة بالهُوية العربية الإسلامية، وتعتبر الدين الإسلامي دين أغلبية التونسيين .
                                <br>
                                <br>
                                –  تضمن الدولة حرية المعتقد للجميع ما لم تُخل بالأمن العام .
                                <br>
                                <br>
                                –  توفر الدولة أماكن العبادة وتشرف عليها .
                                <br>
                                <br>
                                –  تعمل الدولة على تحييد أماكن العبادة عن العمل السياسي والدعاية الحزبية .
                                <br>
                                <br>
                                –  تضمن الدولة تعليم التربية الدينية في المدارس والمعاهد العمومية .
                                <br>
                                <br>
                                •  في صورة التنازع بين مصلحة الدولة و مصلحة الحزب تعطى الأولوية لمصلحة الدولة .
                                <br>
                                <br>
                                •  يلتزم الحزب بسياسة المصارحة و الشفافية مع منخرطيه و سائر المواطنين، و يتعامل معهم على أساس كونهم مسؤولين .
                                <br>
                                <br>
                                •  يعمل الحزب على إحداث تغيير في بعض الظواهر الثقافية السلبية للتونسيين دون تدخل في الحياة الخاصة للناس و نمط حياتهم و اختياراتهم الشخصية ، و يواجه الناس بالحقيقة قدر الإمكان حتّى وإن اقتضى الأمر خسارة أصواتهم .
                                <br>
                                <br>
                                •  يعتبر الحزب مكافحة الفساد من أولويات الدولة في كل المؤسسات، ويعمل على استئصال هذه الظاهرة أو التقليص منها إلى أدنى حد، ولا يتسامح مع الفساد و خاصة عندما يتورط فيه المنخرطون فيه أو المتحملون للمسؤولية في الدولة.
                                <br>
                                <br>
                                •  يسعى الحزب إلى أن يكون حزبا قويا يصل إلى السلطة و ذلك بتحسين إدارته و هيكلته و تسييره بشكل ديمقراطي وباستقطابه للكفاءات و لعدد كبير من المنخرطين النوعيين يشترط فيهم حسن السلوك و الحس الوطني . و في صورة عدم الوصول إلى السلطة يضطلع الحزب بدور المعارضة الفعّالة و المسؤولة التي تنقد و تقدم البدائل دون أن تصل إلى سياسة العرقلة أو الإرباك ما دامت الحكومة ملتزمة بالقوانين .
                                <br>
                                <br>
                                •  يمكن للحزب الدخول في تحالفات أو التنسيق مع أحزاب أخرى غير مورّطة مع نظام الاستبداد، و غير متورّطة في الفساد، ولا تتبنى أفكارا مخالفة لقيم الجمهورية .
                                <br>
                                <br>
                                •  حزبنا غير معاد للإيديولوجيات و الأفكار المخالفة التي تتبناها الأحزاب و المنظمات الأخرى ما دامت ملتزمة بالدستور والقوانين، وهو يتميز بالتسامح و عدم رفض الآخر.
                                <br>
                                <br>
                                •  يعمل الحزب من أجل التهيئة لتحقيق مشروع إقامة دولة فدرالية عربية تجمع بين الشعوب العربية المتحررة من الاستبداد .
                                <br>
                                <br>
                                •  يعمل الحزب على تكريس سياسة خارجية للدولة تقوم على حسن العلاقات مع سائر الدول، وعدم التدخل في شؤون الغير، مع عدم السكوت عن الانتهاكات البليغة لحقوق الإنسان، وترفض التطبيع مع الكيان الصهيوني، وتساهم في إيجاد حل عادل للقضية الفلسطينية .
                                <br>
                                <br>
                                •  يعمل الحزب على أن تكون الانتخابات نزيهة و شفافة و معبّرة عن إرادة الشعب حريته، و يسعى إلى أن تتم في مناخ سلمي و آمن، وفي ظل حياد لإدارة، وألاّ تُستغل الإمكانيات المالية لأي طرف في شراء الأصوات، أو توظيف إمكانيات الدولة لاستمالة المواطنين، و يطالب الحزب بالرقابة المالية على الأحزاب السياسية والجمعيات والمؤسسات الإعلامية .
                                <br>
                                <br>
                                •  يعمل الحزب على تكريس أكبر قدر من الشفافية في إدارة الشأن العام، وحق المواطن في الحصول على المعلومة .

                                عاشت تونس .. عاش الشعب التونسي حرا أبيا
                            </div>
                        </div>
                    </div>
                    <div class="col-5 col-sm-3">
                        <div class="nav flex-column nav-tabs nav-tabs-right h-100" id="vert-tabs-right-tab" role="tablist" aria-orientation="vertical">
                            <a class="nav-link active" id="vert-tabs-right-home-tab" data-toggle="pill" href="#vert-tabs-right-home-courant" role="tab" aria-controls="vert-tabs-right-home" aria-selected="true">البيان التاسيسي</a>
                            <a class="nav-link" id="vert-tabs-right-profile-tab" data-toggle="pill" href="#vert-tabs-right-profile-courant" role="tab" aria-controls="vert-tabs-right-profile" aria-selected="false">النظام التاسيسي</a>
                            <a class="nav-link" id="vert-tabs-right-messages-tab" data-toggle="pill" href="#vert-tabs-right-messages-courant" role="tab" aria-controls="vert-tabs-right-messages" aria-selected="false">النظام الداخلي للحزب</a>
                            <a class="nav-link" id="vert-tabs-right-settings-tab" data-toggle="pill" href="#vert-tabs-right-settings-courant" role="tab" aria-controls="vert-tabs-right-settings" aria-selected="false">الهوية و الخط السياسي</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card -->
        </div>
    </div>
</div>

@endsection
