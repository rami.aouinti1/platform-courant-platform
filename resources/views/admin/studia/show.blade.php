@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.studium.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.studia.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.studium.fields.id') }}
                        </th>
                        <td>
                            {{ $studium->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.studium.fields.ecole') }}
                        </th>
                        <td>
                            {{ $studium->ecole }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.studium.fields.branche') }}
                        </th>
                        <td>
                            {{ $studium->branche }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.studium.fields.degree') }}
                        </th>
                        <td>
                            {{ App\Studium::DEGREE_SELECT[$studium->degree] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.studium.fields.date_beginn') }}
                        </th>
                        <td>
                            {{ $studium->date_beginn }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.studium.fields.date_end') }}
                        </th>
                        <td>
                            {{ $studium->date_end }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.studia.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection