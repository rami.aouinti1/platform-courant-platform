@extends('layouts.admin')
@section('content')
@can('studium_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.studia.create") }}">
                {{ trans('global.add') }} {{ trans('cruds.studium.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.studium.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Studium">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.studium.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.studium.fields.ecole') }}
                        </th>
                        <th>
                            {{ trans('cruds.studium.fields.branche') }}
                        </th>
                        <th>
                            {{ trans('cruds.studium.fields.degree') }}
                        </th>
                        <th>
                            {{ trans('cruds.studium.fields.date_beginn') }}
                        </th>
                        <th>
                            {{ trans('cruds.studium.fields.date_end') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($studia as $key => $studium)
                        <tr data-entry-id="{{ $studium->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $studium->id ?? '' }}
                            </td>
                            <td>
                                {{ $studium->ecole ?? '' }}
                            </td>
                            <td>
                                {{ $studium->branche ?? '' }}
                            </td>
                            <td>
                                {{ App\Studium::DEGREE_SELECT[$studium->degree] ?? '' }}
                            </td>
                            <td>
                                {{ $studium->date_beginn ?? '' }}
                            </td>
                            <td>
                                {{ $studium->date_end ?? '' }}
                            </td>
                            <td>
                                @can('studium_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.studia.show', $studium->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

                                @can('studium_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.studia.edit', $studium->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('studium_delete')
                                    <form action="{{ route('admin.studia.destroy', $studium->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('studium_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.studia.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  $('.datatable-Studium:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection