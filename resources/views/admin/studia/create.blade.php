@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.studium.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.studia.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="ecole">{{ trans('cruds.studium.fields.ecole') }}</label>
                <input class="form-control {{ $errors->has('ecole') ? 'is-invalid' : '' }}" type="text" name="ecole" id="ecole" value="{{ old('ecole', '') }}">
                @if($errors->has('ecole'))
                    <span class="text-danger">{{ $errors->first('ecole') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.studium.fields.ecole_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="branche">{{ trans('cruds.studium.fields.branche') }}</label>
                <input class="form-control {{ $errors->has('branche') ? 'is-invalid' : '' }}" type="text" name="branche" id="branche" value="{{ old('branche', '') }}">
                @if($errors->has('branche'))
                    <span class="text-danger">{{ $errors->first('branche') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.studium.fields.branche_helper') }}</span>
            </div>
            <div class="form-group">
                <label>{{ trans('cruds.studium.fields.degree') }}</label>
                <select class="form-control {{ $errors->has('degree') ? 'is-invalid' : '' }}" name="degree" id="degree">
                    <option value disabled {{ old('degree', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                    @foreach(App\Studium::DEGREE_SELECT as $key => $label)
                        <option value="{{ $key }}" {{ old('degree', '') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                    @endforeach
                </select>
                @if($errors->has('degree'))
                    <span class="text-danger">{{ $errors->first('degree') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.studium.fields.degree_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="date_beginn">{{ trans('cruds.studium.fields.date_beginn') }}</label>
                <input class="form-control date {{ $errors->has('date_beginn') ? 'is-invalid' : '' }}" type="text" name="date_beginn" id="date_beginn" value="{{ old('date_beginn') }}">
                @if($errors->has('date_beginn'))
                    <span class="text-danger">{{ $errors->first('date_beginn') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.studium.fields.date_beginn_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="date_end">{{ trans('cruds.studium.fields.date_end') }}</label>
                <input class="form-control date {{ $errors->has('date_end') ? 'is-invalid' : '' }}" type="text" name="date_end" id="date_end" value="{{ old('date_end') }}">
                @if($errors->has('date_end'))
                    <span class="text-danger">{{ $errors->first('date_end') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.studium.fields.date_end_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection