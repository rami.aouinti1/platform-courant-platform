@extends('layouts.admin')
@section('content')
@can('profile_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.profiles.create") }}">
                {{ trans('global.add') }} {{ trans('cruds.profile.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.profile.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Profile">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.profile.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.profile.fields.firstname') }}
                        </th>
                        <th>
                            {{ trans('cruds.profile.fields.lastname') }}
                        </th>
                        <th>
                            {{ trans('cruds.profile.fields.date_of_birthday') }}
                        </th>
                        <th>
                            {{ trans('cruds.profile.fields.address') }}
                        </th>
                        <th>
                            {{ trans('cruds.profile.fields.telephone') }}
                        </th>
                        <th>
                            {{ trans('cruds.profile.fields.photo') }}
                        </th>
                        <th>
                            {{ trans('cruds.profile.fields.date_beginn') }}
                        </th>
                        <th>
                            {{ trans('cruds.profile.fields.date_end') }}
                        </th>
                        <th>
                            {{ trans('cruds.profile.fields.active') }}
                        </th>
                        <th>
                            {{ trans('cruds.profile.fields.sexe') }}
                        </th>
                        <th>
                            {{ trans('cruds.profile.fields.status') }}
                        </th>
                        <th>
                            {{ trans('cruds.profile.fields.user') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($profiles as $key => $profile)
                        <tr data-entry-id="{{ $profile->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $profile->id ?? '' }}
                            </td>
                            <td>
                                {{ $profile->firstname ?? '' }}
                            </td>
                            <td>
                                {{ $profile->lastname ?? '' }}
                            </td>
                            <td>
                                {{ $profile->date_of_birthday ?? '' }}
                            </td>
                            <td>
                                {{ $profile->address ?? '' }}
                            </td>
                            <td>
                                {{ $profile->telephone ?? '' }}
                            </td>
                            <td>
                                @if($profile->photo)
                                    <a href="{{ $profile->photo->getUrl() }}" target="_blank">
                                        <img src="{{ $profile->photo->getUrl('thumb') }}" width="50px" height="50px">
                                    </a>
                                @endif
                            </td>
                            <td>
                                {{ $profile->date_beginn ?? '' }}
                            </td>
                            <td>
                                {{ $profile->date_end ?? '' }}
                            </td>
                            <td>
                                <span style="display:none">{{ $profile->active ?? '' }}</span>
                                <input type="checkbox" disabled="disabled" {{ $profile->active ? 'checked' : '' }}>
                            </td>
                            <td>
                                {{ App\Profile::SEXE_SELECT[$profile->sexe] ?? '' }}
                            </td>
                            <td>
                                {{ App\Profile::STATUS_SELECT[$profile->status] ?? '' }}
                            </td>
                            <td>
                                {{ $profile->user->name ?? '' }}
                            </td>
                            <td>
                                @can('profile_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.profiles.show', $profile->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

                                @can('profile_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.profiles.edit', $profile->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('profile_delete')
                                    <form action="{{ route('admin.profiles.destroy', $profile->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('profile_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.profiles.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  $('.datatable-Profile:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection