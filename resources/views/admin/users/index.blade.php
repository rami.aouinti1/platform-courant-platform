@extends('layouts.admin')
@section('content')
@can('user_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.users.create") }}">
                {{ trans('global.add') }} {{ trans('cruds.user.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.user.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.user.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.name') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.firstname') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.lastname') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.email') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.email_verified_at') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.roles') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.photo') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.date_of_birthday') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.carte_number') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.country') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.state') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.street') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.street_number') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.telephone') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.date_beginn') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.sexe') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.status') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.studium') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.experience') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.certificate') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.others') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $key => $user)
                        <tr data-entry-id="{{ $user->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $user->id ?? '' }}
                            </td>
                            <td>
                                {{ $user->name ?? '' }}
                            </td>
                            <td>
                                {{ $user->firstname ?? '' }}
                            </td>
                            <td>
                                {{ $user->lastname ?? '' }}
                            </td>
                            <td>
                                {{ $user->email ?? '' }}
                            </td>
                            <td>
                                {{ $user->email_verified_at ?? '' }}
                            </td>
                            <td>
                                @foreach($user->roles as $key => $item)
                                    <span class="badge badge-info">{{ $item->title }}</span>
                                @endforeach
                            </td>
                            <td>
                                @if($user->photo)
                                    <a href="{{ $user->photo->getUrl() }}" target="_blank">
                                        <img src="{{ $user->photo->getUrl('thumb') }}" width="50px" height="50px">
                                    </a>
                                @endif
                            </td>
                            <td>
                                {{ $user->date_of_birthday ?? '' }}
                            </td>
                            <td>
                                {{ $user->carte_number ?? '' }}
                            </td>
                            <td>
                                {{ App\User::COUNTRY_SELECT[$user->country] ?? '' }}
                            </td>
                            <td>
                                {{ $user->state ?? '' }}
                            </td>
                            <td>
                                {{ $user->street ?? '' }}
                            </td>
                            <td>
                                {{ $user->street_number ?? '' }}
                            </td>
                            <td>
                                {{ $user->telephone ?? '' }}
                            </td>
                            <td>
                                {{ $user->date_beginn ?? '' }}
                            </td>
                            <td>
                                {{ App\User::SEXE_SELECT[$user->sexe] ?? '' }}
                            </td>
                            <td>
                                {{ App\User::STATUS_SELECT[$user->status] ?? '' }}
                            </td>
                            <td>
                                @foreach($user->studia as $key => $item)
                                    <span class="badge badge-info">{{ $item->ecole }}</span>
                                @endforeach
                            </td>
                            <td>
                                @foreach($user->experiences as $key => $item)
                                    <span class="badge badge-info">{{ $item->company_name }}</span>
                                @endforeach
                            </td>
                            <td>
                                @foreach($user->certificates as $key => $item)
                                    <span class="badge badge-info">{{ $item->certificate }}</span>
                                @endforeach
                            </td>
                            <td>
                                @foreach($user->others as $key => $item)
                                    <span class="badge badge-info">{{ $item->title }}</span>
                                @endforeach
                            </td>
                            <td>
                                @can('user_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.users.show', $user->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

                                @can('user_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.users.edit', $user->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('user_delete')
                                    <form action="{{ route('admin.users.destroy', $user->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('user_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.users.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  $('.datatable-User:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection