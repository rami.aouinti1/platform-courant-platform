@extends('layouts.admin')
@section('content')
    <div class="card card-solid">
        <div class="card-body pb-0">
            <div class="row d-flex align-items-stretch">
                @foreach($users as $key => $user)
                <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">
                    <div class="card bg-light">
                        <div class="card-header text-muted border-bottom-0">
                        </div>
                        <div class="card-body pt-0">
                            <div class="row">
                                <div class="col-7">
                                    <h2 class="lead"><b>{{ $user->firstname ?? '' }}  {{ $user->lastname ?? '' }} </b></h2>
                                    <p class="text-muted text-sm"><b>Email: </b> {{ $user->email ?? '' }} </p>
                                    <ul class="ml-4 mb-0 fa-ul text-muted">
                                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span> Address: {{ $user->street ?? '' }}  {{ $user->street_number ?? '' }} {{ $user->state ?? '' }}</li> <br>
                                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span> Phone #: {{ $user->telephone ?? '' }}</li>
                                    </ul>
                                </div>
                                <div class="col-5 text-center">
                                    @if($user->photo)
                                    <img src="{{ $user->photo->getUrl() }}" alt="" class="img-circle img-fluid">
                                    @else
                                        @if($user->sexe == 'Homme')
                                            <img src="{{URL::asset('dist/img/avatar5.png')}}" alt="" class="img-circle img-fluid">
                                        @else
                                            <img src="{{URL::asset('dist/img/avatar2.png')}}" alt="" class="img-circle img-fluid">
                                        @endif
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="text-right">
                                <a href="#" class="btn btn-sm bg-warning">
                                    <i class="fas fa-comments"></i>
                                </a>
                                <a href="{{ route('admin.users.show', $user->id) }}" class="btn btn-sm btn-warning">
                                    <i class="fas fa-user"></i> {{ trans('global.view') }}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <nav aria-label="Contacts Page Navigation">
                <ul class="pagination justify-content-center m-0">
                    <li class="page-item active"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">4</a></li>
                    <li class="page-item"><a class="page-link" href="#">5</a></li>
                    <li class="page-item"><a class="page-link" href="#">6</a></li>
                    <li class="page-item"><a class="page-link" href="#">7</a></li>
                    <li class="page-item"><a class="page-link" href="#">8</a></li>
                </ul>
            </nav>
        </div>
        <!-- /.card-footer -->
    </div>
@endsection
