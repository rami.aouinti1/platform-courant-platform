@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.user.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.users.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.user.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', '') }}" required>
                @if($errors->has('name'))
                    <span class="text-danger">{{ $errors->first('name') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="firstname">{{ trans('cruds.user.fields.firstname') }}</label>
                <input class="form-control {{ $errors->has('firstname') ? 'is-invalid' : '' }}" type="text" name="firstname" id="firstname" value="{{ old('firstname', '') }}">
                @if($errors->has('firstname'))
                    <span class="text-danger">{{ $errors->first('firstname') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.firstname_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="lastname">{{ trans('cruds.user.fields.lastname') }}</label>
                <input class="form-control {{ $errors->has('lastname') ? 'is-invalid' : '' }}" type="text" name="lastname" id="lastname" value="{{ old('lastname', '') }}">
                @if($errors->has('lastname'))
                    <span class="text-danger">{{ $errors->first('lastname') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.lastname_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="email">{{ trans('cruds.user.fields.email') }}</label>
                <input class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" type="text" name="email" id="email" value="{{ old('email') }}" required>
                @if($errors->has('email'))
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.email_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="password">{{ trans('cruds.user.fields.password') }}</label>
                <input class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" type="password" name="password" id="password" required>
                @if($errors->has('password'))
                    <span class="text-danger">{{ $errors->first('password') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.password_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="roles">{{ trans('cruds.user.fields.roles') }}</label>
                <div style="padding-bottom: 4px">
                    <span class="btn btn-info btn-xs select-all" style="border-radius: 0">{{ trans('global.select_all') }}</span>
                    <span class="btn btn-info btn-xs deselect-all" style="border-radius: 0">{{ trans('global.deselect_all') }}</span>
                </div>
                <select class="form-control select2 {{ $errors->has('roles') ? 'is-invalid' : '' }}" name="roles[]" id="roles" multiple required>
                    @foreach($roles as $id => $roles)
                        <option value="{{ $id }}" {{ in_array($id, old('roles', [])) ? 'selected' : '' }}>{{ $roles }}</option>
                    @endforeach
                </select>
                @if($errors->has('roles'))
                    <span class="text-danger">{{ $errors->first('roles') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.roles_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="photo">{{ trans('cruds.user.fields.photo') }}</label>
                <div class="needsclick dropzone {{ $errors->has('photo') ? 'is-invalid' : '' }}" id="photo-dropzone">
                </div>
                @if($errors->has('photo'))
                    <span class="text-danger">{{ $errors->first('photo') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.photo_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="date_of_birthday">{{ trans('cruds.user.fields.date_of_birthday') }}</label>
                <input class="form-control date {{ $errors->has('date_of_birthday') ? 'is-invalid' : '' }}" type="text" name="date_of_birthday" id="date_of_birthday" value="{{ old('date_of_birthday') }}">
                @if($errors->has('date_of_birthday'))
                    <span class="text-danger">{{ $errors->first('date_of_birthday') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.date_of_birthday_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="carte_number">{{ trans('cruds.user.fields.carte_number') }}</label>
                <input class="form-control {{ $errors->has('carte_number') ? 'is-invalid' : '' }}" type="number" name="carte_number" id="carte_number" value="{{ old('carte_number') }}" step="1">
                @if($errors->has('carte_number'))
                    <span class="text-danger">{{ $errors->first('carte_number') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.carte_number_helper') }}</span>
            </div>
            <div class="form-group">
                <label>{{ trans('cruds.user.fields.country') }}</label>
                <select class="form-control {{ $errors->has('country') ? 'is-invalid' : '' }}" name="country" id="country">
                    <option value disabled {{ old('country', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                    @foreach(App\User::COUNTRY_SELECT as $key => $label)
                        <option value="{{ $key }}" {{ old('country', '') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                    @endforeach
                </select>
                @if($errors->has('country'))
                    <span class="text-danger">{{ $errors->first('country') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.country_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="state">{{ trans('cruds.user.fields.state') }}</label>
                <input class="form-control {{ $errors->has('state') ? 'is-invalid' : '' }}" type="text" name="state" id="state" value="{{ old('state', '') }}">
                @if($errors->has('state'))
                    <span class="text-danger">{{ $errors->first('state') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.state_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="street">{{ trans('cruds.user.fields.street') }}</label>
                <input class="form-control {{ $errors->has('street') ? 'is-invalid' : '' }}" type="text" name="street" id="street" value="{{ old('street', '') }}">
                @if($errors->has('street'))
                    <span class="text-danger">{{ $errors->first('street') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.street_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="street_number">{{ trans('cruds.user.fields.street_number') }}</label>
                <input class="form-control {{ $errors->has('street_number') ? 'is-invalid' : '' }}" type="text" name="street_number" id="street_number" value="{{ old('street_number', '') }}">
                @if($errors->has('street_number'))
                    <span class="text-danger">{{ $errors->first('street_number') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.street_number_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="telephone">{{ trans('cruds.user.fields.telephone') }}</label>
                <input class="form-control {{ $errors->has('telephone') ? 'is-invalid' : '' }}" type="text" name="telephone" id="telephone" value="{{ old('telephone', '') }}">
                @if($errors->has('telephone'))
                    <span class="text-danger">{{ $errors->first('telephone') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.telephone_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="date_beginn">{{ trans('cruds.user.fields.date_beginn') }}</label>
                <input class="form-control date {{ $errors->has('date_beginn') ? 'is-invalid' : '' }}" type="text" name="date_beginn" id="date_beginn" value="{{ old('date_beginn') }}">
                @if($errors->has('date_beginn'))
                    <span class="text-danger">{{ $errors->first('date_beginn') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.date_beginn_helper') }}</span>
            </div>
            <div class="form-group">
                <label>{{ trans('cruds.user.fields.sexe') }}</label>
                <select class="form-control {{ $errors->has('sexe') ? 'is-invalid' : '' }}" name="sexe" id="sexe">
                    <option value disabled {{ old('sexe', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                    @foreach(App\User::SEXE_SELECT as $key => $label)
                        <option value="{{ $key }}" {{ old('sexe', 'Homme') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                    @endforeach
                </select>
                @if($errors->has('sexe'))
                    <span class="text-danger">{{ $errors->first('sexe') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.sexe_helper') }}</span>
            </div>
            <div class="form-group">
                <label>{{ trans('cruds.user.fields.status') }}</label>
                <select class="form-control {{ $errors->has('status') ? 'is-invalid' : '' }}" name="status" id="status">
                    <option value disabled {{ old('status', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                    @foreach(App\User::STATUS_SELECT as $key => $label)
                        <option value="{{ $key }}" {{ old('status', '') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                    @endforeach
                </select>
                @if($errors->has('status'))
                    <span class="text-danger">{{ $errors->first('status') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.status_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="studia">{{ trans('cruds.user.fields.studium') }}</label>
                <div style="padding-bottom: 4px">
                    <span class="btn btn-info btn-xs select-all" style="border-radius: 0">{{ trans('global.select_all') }}</span>
                    <span class="btn btn-info btn-xs deselect-all" style="border-radius: 0">{{ trans('global.deselect_all') }}</span>
                </div>
                <select class="form-control select2 {{ $errors->has('studia') ? 'is-invalid' : '' }}" name="studia[]" id="studia" multiple>
                    @foreach($studia as $id => $studium)
                        <option value="{{ $id }}" {{ in_array($id, old('studia', [])) ? 'selected' : '' }}>{{ $studium }}</option>
                    @endforeach
                </select>
                @if($errors->has('studia'))
                    <span class="text-danger">{{ $errors->first('studia') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.studium_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="experiences">{{ trans('cruds.user.fields.experience') }}</label>
                <div style="padding-bottom: 4px">
                    <span class="btn btn-info btn-xs select-all" style="border-radius: 0">{{ trans('global.select_all') }}</span>
                    <span class="btn btn-info btn-xs deselect-all" style="border-radius: 0">{{ trans('global.deselect_all') }}</span>
                </div>
                <select class="form-control select2 {{ $errors->has('experiences') ? 'is-invalid' : '' }}" name="experiences[]" id="experiences" multiple>
                    @foreach($experiences as $id => $experience)
                        <option value="{{ $id }}" {{ in_array($id, old('experiences', [])) ? 'selected' : '' }}>{{ $experience }}</option>
                    @endforeach
                </select>
                @if($errors->has('experiences'))
                    <span class="text-danger">{{ $errors->first('experiences') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.experience_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="certificates">{{ trans('cruds.user.fields.certificate') }}</label>
                <div style="padding-bottom: 4px">
                    <span class="btn btn-info btn-xs select-all" style="border-radius: 0">{{ trans('global.select_all') }}</span>
                    <span class="btn btn-info btn-xs deselect-all" style="border-radius: 0">{{ trans('global.deselect_all') }}</span>
                </div>
                <select class="form-control select2 {{ $errors->has('certificates') ? 'is-invalid' : '' }}" name="certificates[]" id="certificates" multiple>
                    @foreach($certificates as $id => $certificate)
                        <option value="{{ $id }}" {{ in_array($id, old('certificates', [])) ? 'selected' : '' }}>{{ $certificate }}</option>
                    @endforeach
                </select>
                @if($errors->has('certificates'))
                    <span class="text-danger">{{ $errors->first('certificates') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.certificate_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="others">{{ trans('cruds.user.fields.others') }}</label>
                <div style="padding-bottom: 4px">
                    <span class="btn btn-info btn-xs select-all" style="border-radius: 0">{{ trans('global.select_all') }}</span>
                    <span class="btn btn-info btn-xs deselect-all" style="border-radius: 0">{{ trans('global.deselect_all') }}</span>
                </div>
                <select class="form-control select2 {{ $errors->has('others') ? 'is-invalid' : '' }}" name="others[]" id="others" multiple>
                    @foreach($others as $id => $others)
                        <option value="{{ $id }}" {{ in_array($id, old('others', [])) ? 'selected' : '' }}>{{ $others }}</option>
                    @endforeach
                </select>
                @if($errors->has('others'))
                    <span class="text-danger">{{ $errors->first('others') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.others_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection

@section('scripts')
<script>
    Dropzone.options.photoDropzone = {
    url: '{{ route('admin.users.storeMedia') }}',
    maxFilesize: 2, // MB
    acceptedFiles: '.jpeg,.jpg,.png,.gif',
    maxFiles: 1,
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 2,
      width: 4096,
      height: 4096
    },
    success: function (file, response) {
      $('form').find('input[name="photo"]').remove()
      $('form').append('<input type="hidden" name="photo" value="' + response.name + '">')
    },
    removedfile: function (file) {
      file.previewElement.remove()
      if (file.status !== 'error') {
        $('form').find('input[name="photo"]').remove()
        this.options.maxFiles = this.options.maxFiles + 1
      }
    },
    init: function () {
@if(isset($user) && $user->photo)
      var file = {!! json_encode($user->photo) !!}
          this.options.addedfile.call(this, file)
      this.options.thumbnail.call(this, file, '{{ $user->photo->getUrl('thumb') }}')
      file.previewElement.classList.add('dz-complete')
      $('form').append('<input type="hidden" name="photo" value="' + file.file_name + '">')
      this.options.maxFiles = this.options.maxFiles - 1
@endif
    },
    error: function (file, response) {
        if ($.type(response) === 'string') {
            var message = response //dropzone sends it's own error messages in string
        } else {
            var message = response.errors.file
        }
        file.previewElement.classList.add('dz-error')
        _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
        _results = []
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            node = _ref[_i]
            _results.push(node.textContent = message)
        }

        return _results
    }
}
</script>
@endsection